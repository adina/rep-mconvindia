# Rep-MConvIndia

README file for reproducing results in:  
**The convergence history of India-Eurasia records multiple subduction dynamics processes**  
Citation: Pusok, A.E., and Stegman, D.R. (2020) The convergence history of India-Eurasia records multiple subduction dynamics processes, *Science Advances*, Vol. 6, no. 19, eaaz8681, doi: 10.1126/sciadv.aaz8681

https://advances.sciencemag.org/content/6/19/eaaz8681

Authors: Adina E. Pusok*, Dave R. Stegman  
Affiliation: Scripps Institution of Oceanography, UCSD  
A.E.P now at Department of Earth Sciences, University of Oxford, UK

Corresponding author: adina.pusok@earth.ox.ac.uk (A.E. Pusok).

### Structure of Repository [733 Mb] ###
     
* Convergence\_data [31.8 Mb] - materials used in the data compilation
* Publication [16.6 Mb] - manuscript and supplementary data
* Figures [683.1 Mb] - time evolution for all simulations (viscosity maps and velocity streamlines), archived
* Input_Files [1.3 Mb] - input files for all simulations to run with LaMEM (see details below)
* Model\_Setup [298 kb] - files to generate the initial input geometry as particles (see details below)

### Convergence data ###
*Convergence\_data/* contains the following structure:

* analysis - overview of all data colected
* digitized\_plots - data digitized with [WebPlotDigitizer](https://apps.automeris.io/wpd). The data sets used in figure 1B are from Cande and Stegman (2011) in the following files:
     - n022_cande_stegman_2011_fig2_ind_ant.csv
     - n023_cande_stegman_2011_fig2_ind_afr.csv
* screenshot - screenshot figures of collected data from original publications
* matlab_scripts:
     - analysis01: plot every data set as in the Supplementary material, 
     - analysis02: reproduce figure 2a,b, 
     - analysis03: same as analysis01, but some data sets are collapsed together on the same plot (i.e. shiplines), 
     - analysis04: cluster analysis of data.
* figures - the output from the matlab scripts located in \matlab_scripts

### Code and dependencies ###

[LaMEM](https://bitbucket.org/bkaus/lamem/branch/cvi_test) version and dependencies (Kaus et al., 2016).
LaMEM version used: cvi\_test branch latest commit

LaMEM source code/branches are not provided in this archive! Please download them from the links provided.
LaMEM and the cvi_branch can be downloaded from Bitbucket using ‘git clone’.

The following dependencies are needed to install LaMEM and reproduce results: petsc3.7.5, gcc6 and mpi compilers (available through Macports on MAC OSX). Installation details are located in: LaMEM (cvi_test)/doc/installation, and in the LaMEM repository wiki page.

### Model Setup ###

The model setups (i.e particle files) were created in Matlab. The Matlab scripts for this study can be found in Model\_Setup/. Executing them will produce the particle files for the different geometries needed. <br/>
Preview of initial geometry is possible in Paraview with the option:  
Paraview_output = 1;

Simulations were performed on 4-16 cpus (2-D) on Comet (SDSC).

### Input Files ###

(LaMEM Parameter Files)

Every simulation can be reproduced by running each parameter file with the respective particle input geometry.
The input geometry obtained before is specified in each file as:  
LoadInitialParticlesDirectory  = ../../Input/MatlabInputParticles

General command to run LaMEM (may change on high-performance clusters):  
mpiexec -n 16 ./*path to exec*/LaMEM -ParamFile Setup_IndiaAsia_2D -restart 1

### Visualization and Post-processing ###

Performed in Paraview and Matlab. *Figures/* contains low-resolution output files for every simulation in this study.
Please contact the main author for questions and access regarding the post-processing work.

### Extended Archive (request from A.E.P.)###

