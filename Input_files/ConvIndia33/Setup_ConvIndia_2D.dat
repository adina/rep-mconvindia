# -----------------------------------------------------------------------------------------------------------------------
# A basic parameter file for FDSTAG - Subduction 3-D Subduction setup - use CreatePhases.m to generate the input geometry  
# -----------------------------------------------------------------------------------------------------------------------

# ----------------------------------------------------
# Number of elements in x,y,z-direction 
# ----------------------------------------------------

nel_x						=   	-6  #1024  
nel_y						= 	2   
nel_z						= 	-2  #256

# Variable grid spacing
seg_x = 1000 2000 3000 4000 5000 80 216 216 216 216 80 1.0 0.5 2.0 0.5 2.0 1.0
seg_z = -900 64 192 0.5 1.0

# Define number of elements in x,y & z-direction
# Negative number implies corresponding number of mesh segments
# Data is provided in the variables seg_x, seg_y, seg_z and includes:
#    * coordinates of the delimiters between the segments (n-1 points)
#    * number of cells                                    (n points)
#    * bias coefficients                                  (n points) 

# ----------------------------------------------------
# Geometry 
# ----------------------------------------------------
Gravity						=	-10		# [m/s2] - gravitational acceleration 
DimensionalUnits				=	1		# Dimensionless (=0) or SI-dimensional units (=1)? 
units						=	geo

Characteristic.Length		                =	1e6		# [m]		
Characteristic.Viscosity 	                =	1e20		# [Pa.s]
Characteristic.Temperature			=	1000		# [K]
Characteristic.Stress      			=   	1e12		# [Pa]

L						=	10		# Length (y-direction) [km]
W						=	6000		# Width  (x-direction) [km]
H						=	2048		# Height (z-direction) [km]
x_left						=	0.0		# Left side of domain [km]
y_front						=	0.0		# Front side of domain [km]
z_bot						=	-1988		# Bottom of box [km]

NumPartX					=	3		#  Particles/Cell in x-direction
NumPartY					=	3		#  Particles/Cell in y-direction
NumPartZ					=	3		#  Particles/Cell in z-direction

# ----------------------------------------------------
# Model Setup
# ----------------------------------------------------
msetup                         = parallel
ParticleFilename               = Particles				# File that contains markers distribution (matlab)
LoadInitialParticlesDirectory  = ../Input/MatlabInputParticles-DoubleLAY    # directory that contains markers distribution (matlab)

# ----------------------------------------------------
# Input/Output and Numerics
# ----------------------------------------------------

OutputFile					=	Subd2D_Output		
save_timesteps					=	20		# save every X timesteps
time_end					=	1501		# last timestep
save_breakpoints				= 	50		#  -1: don't save breakpoints + initial particles
									#   0: save initial particles only !
									# 0+n: after how many timesteps we save a breakpointfile
SaveParticles 				        =	0		# save particles files yes (1) or no (0) 
CFL						=	0.5		# Courant-criteria dt = min(dx,dy,dz)/max(Vx,Vy,Vz)*CFL (0.5-1.0)
dt_max						=	1e-1		# Maximum timestep [Myr]
tstop						=	9.0		# Maximum time [Myr]
									
# ----------------------------------------------------
# Boundary Conditions
# ----------------------------------------------------

BC.Eyy						=	0.0e-15		# BG strainrate in y-direction [1/s]
BC.Exx						=	0.0e-15		# BG Strainrate in x-direction [1/s]
BC.LeftBound					=	1		# 0 - free surface, 1-free slip with BG strainrate Eyy, 2 - no slip
BC.RightBound					=	1		# 0 - free surface, 1-free slip with BG strainrate Eyy, 2 - no slip
BC.FrontBound					=	1		# 0 - free surface, 1-free slip with BG strainrate Exx, 2 - no slip
BC.BackBound					=	1		# 0 - free surface, 1-free slip with BG strainrate Exx, 2 - no slip
BC.LowerBound					=	1		# 0 - free surface, 1-free slip, 2 - no slip
BC.UpperBound					=	1		# 0 - free surface, 1-free slip, 2 - no slip
Temp_top	    				=   	0		# Temperature @ top [deg C]
Temp_bottom					=   	0		# Temperature @ bottom; side BC's are flux-free by default [deg C]

# ----------------------------------------------------
# Pushing Boundary Conditions
# ----------------------------------------------------
AddPushing			=	0				# 1 - pushing; 0 - no pushing;
Pushing.num_changes		=	1				# no. of changes in the pushing direction
Pushing.time			=	0 100				# Time segments [Myr] as an array
Pushing.V_push			=	5			        # [cm/yr] as an array
Pushing.dir			= 	1				# preferred direction of pushing: 0-rotation, 1-Vx direction, 2-Vy direction
Pushing.omega			=	0 			  	# rate of rotation [deg/Myr] as an array
Pushing.coord_advect		=	1 				# 0 - fixed pushing block, 1 - moving pushing block
Pushing.reset_pushing_coord	= 	0;				# 0 - no reset, 1 - reset pushing coord every timestep
#Pushing.theta			= 	0;				# angle from which rotation should start

Pushing.L_block					=	300		# Length (x-direction) [km]
Pushing.W_block					=	2400		# Width  (y-direction) [km]
Pushing.H_block					=	60		# Height (z-direction) [km]

Pushing.x_center_block				=	630		# Coordinates of the center if the block [km]
Pushing.y_center_block				=	2560
Pushing.z_center_block 				=	934

# ----------------------------------------------------
# Other Properties 
# ----------------------------------------------------		

# --- Optimization parameters ---
FSSA						= 	1.0		# The "free surface stabilization algorithm" - for sticky air

# --- Viscosity Cutoff ---
LowerViscosityCutoff				=	1e18		# [Pa.s]
UpperViscosityCutoff				=	1e25		# [Pa.s]
DII_ref						=	1e-16

# ----------------------------------------------------
# Phases and Material Properties
# ----------------------------------------------------

  	# --------------------------
	# MANTLE ASTHENOSPHERE [0]
  	# --------------------------
	<MaterialStart>
		ID 			=	0	# phase id  [-]
		rho0        		= 	3300	# density - if dimensional [kg/m3]
		eta			=	2.8e20
	<MaterialEnd>

  	# --------------------------
	# AIR [1]
  	# --------------------------
	<MaterialStart>
		ID 			=	1	# phase id  [-]
		rho0        		= 	1	# density - if dimensional [kg/m3]
		eta			= 	1e18	# diffusion creep - ref.  viscosit
	<MaterialEnd>

  	# --------------------------
	# SLAB Left Subduction [2]
  	# --------------------------
	<MaterialStart>
		ID 			=	2	# phase id  [-]
		rho0        		= 	3385	# density - if dimensional [kg/m3]
		eta			=	1.4e23
	<MaterialEnd>

  	# --------------------------
	# weak CRUST Left Subduction [3]
  	# --------------------------
	<MaterialStart>
		ID 			=	3	# phase id  [-]
		rho0        		= 	3385	# density - if dimensional [kg/m3]
		eta			=	2.8e20
	<MaterialEnd>	

  	# --------------------------
	# strong CORE Left Subduction [4]
  	# --------------------------
	<MaterialStart>
		ID 			=	4	# phase id  [-]
		rho0        		= 	3385	# density - if dimensional [kg/m3]
		eta			=	1.4e24
	<MaterialEnd>

  	# --------------------------
	# SLAB Right Subduction [5]
  	# --------------------------
	<MaterialStart>
		ID 			=	5	# phase id  [-]
		rho0        		= 	3385	# density - if dimensional [kg/m3]
		eta			=	1.4e23
	<MaterialEnd>

  	# --------------------------
	# weak CRUST Right Subduction [6]
  	# --------------------------
	<MaterialStart>
		ID 			=	6	# phase id  [-]
		rho0        		= 	3385	# density - if dimensional [kg/m3]
		eta			=	2.8e20
	<MaterialEnd>	

  	# --------------------------
	# strong CORE Right Subduction [7]
  	# --------------------------
	<MaterialStart>
		ID 			=	7	# phase id  [-]
		rho0        		= 	3385	# density - if dimensional [kg/m3]
		eta			=	1.4e24
	<MaterialEnd>

  	# --------------------------
	# SLAB Upper Plate [8]
  	# --------------------------
	<MaterialStart>
		ID 			=	8	# phase id  [-]
		rho0        		= 	3385	# density - if dimensional [kg/m3]
		eta			=	1.4e23
	<MaterialEnd>

  	# --------------------------
	# strong CORE Upper Plate [9]
  	# --------------------------
	<MaterialStart>
		ID 			=	9	# phase id  [-]
		rho0        		= 	3385	# density - if dimensional [kg/m3]
		eta			=	1.4e24
	<MaterialEnd>

  	# --------------------------
	# weak ZONE [10]
  	# --------------------------
	<MaterialStart>
		ID 			=	10	# phase id  [-]
		rho0        		= 	3385	# density - if dimensional [kg/m3]
		eta			=	2.8e20
	<MaterialEnd>	

  	# --------------------------
	# MARK1 [11] - LS same as strong core
  	# --------------------------
	<MaterialStart>
		ID 			=	11	# phase id  [-]
		rho0        		= 	3385	# density - if dimensional [kg/m3]
		eta			=	1.4e24
	<MaterialEnd>

  	# --------------------------
	# MARK2 [12] - RS same as strong core
  	# --------------------------
	<MaterialStart>
		ID 			=	12	# phase id  [-]
		rho0        		= 	3385	# density - if dimensional [kg/m3]
		eta			=	1.4e24
	<MaterialEnd>

  	# --------------------------
	# MARK3 [13] - UP same as strong core
  	# --------------------------
	<MaterialStart>
		ID 			=	13	# phase id  [-]
		rho0        		= 	3385	# density - if dimensional [kg/m3]
		eta			=	1.4e24
	<MaterialEnd>

  	# --------------------------
	# SLAB WEAK Left Subduction [14]
  	# --------------------------
	<MaterialStart>
		ID 			=	14	# phase id  [-]
		rho0        		= 	3385	# density - if dimensional [kg/m3]
		eta			=	1.4e23
	<MaterialEnd>

 	# --------------------------
	# SLAB WEAK Right Subduction [15]
  	# --------------------------
	<MaterialStart>
		ID 			=	15	# phase id  [-]
		rho0        		= 	3385	# density - if dimensional [kg/m3]
		eta			=	1.4e23
	<MaterialEnd>

	# --------------------------
	# LOWER MANTLE [16] - Layer 2
  	# --------------------------
	<MaterialStart>
		ID 			=	16	# phase id  [-]
		rho0        		= 	3325	# density - if dimensional [kg/m3]
		eta			=	0.67104e20
	<MaterialEnd>	

	# --------------------------
	# Layer 3 [17]
  	# --------------------------
	<MaterialStart>
		ID 			=	17	# phase id  [-]
		rho0        		= 	3350	# density - if dimensional [kg/m3]
		eta			=	0.53933e20
	<MaterialEnd>
	
	# --------------------------
	# Layer 4 [18]
  	# --------------------------
	<MaterialStart>
		ID 			=	18	# phase id  [-]
		rho0        		= 	3375	# density - if dimensional [kg/m3]
		eta			=	0.98942e20
	<MaterialEnd>	

	# --------------------------
	# Layer 5 [19]
  	# --------------------------
	<MaterialStart>
		ID 			=	19	# phase id  [-]
		rho0        		= 	3400	# density - if dimensional [kg/m3]
		eta			=	4.2327e20
	<MaterialEnd>

	# --------------------------
	# Layer 6 [20]
  	# --------------------------
	<MaterialStart>
		ID 			=	20	# phase id  [-]
		rho0        		= 	3425	# density - if dimensional [kg/m3]
		eta			=	21.899e20
	<MaterialEnd>

	# --------------------------
	# Layer 7 [21]
  	# --------------------------
	<MaterialStart>
		ID 			=	21	# phase id  [-]
		rho0        		= 	3450	# density - if dimensional [kg/m3]
		eta			=	51.4e20
	<MaterialEnd>

	# --------------------------
	# Layer 8 [22]
  	# --------------------------
	<MaterialStart>
		ID 			=	22	# phase id  [-]
		rho0        		= 	3475	# density - if dimensional [kg/m3]
		eta			=	19.18e20
	<MaterialEnd>

	# --------------------------
	# Layer 9 [23]
  	# --------------------------
	<MaterialStart>
		ID 			=	23	# phase id  [-]
		rho0        		= 	3500	# density - if dimensional [kg/m3]
		eta			=	6.6222e20
	<MaterialEnd>

	# --------------------------
	# Layer 10 [24]
  	# --------------------------
	<MaterialStart>
		ID 			=	24	# phase id  [-]
		rho0        		= 	3525	# density - if dimensional [kg/m3]
		eta			=	4.0863e20
	<MaterialEnd>	

# ----------------------------------------------------
# Phase Transitions with Depth
# ----------------------------------------------------
	<PhaseDepthStart>
		ptID 		=	0	# phase transformation id  [-]
		ptdepth         = 	200	# depth [km]
		npt		=	1	# total phase transformations
		phase_up	=	0 	# phase id above ptdepth
		phase_dn	=	16      # phase id below ptdepth
	<PhaseDepthEnd>

	<PhaseDepthStart>
		ptID 		=	1	# phase transformation id  [-]
		ptdepth         = 	400	# depth [km]
		npt		=	3	# total phase transformations
		phase_up	=	3 6 16	# phase id above ptdepth
		phase_dn	=	14 15 17   # phase id below ptdepth
	<PhaseDepthEnd>

	<PhaseDepthStart>
		ptID 		=	2	# phase transformation id  [-]
		ptdepth         = 	600	# depth [km]
		npt		=	1	# total phase transformations
		phase_up	=	17 	# phase id above ptdepth
		phase_dn	=	18      # phase id below ptdepth
	<PhaseDepthEnd>

	<PhaseDepthStart>
		ptID 		=	3	# phase transformation id  [-]
		ptdepth         = 	800	# depth [km]
		npt		=	1	# total phase transformations
		phase_up	=	18 	# phase id above ptdepth
		phase_dn	=	19      # phase id below ptdepth
	<PhaseDepthEnd>

	<PhaseDepthStart>
		ptID 		=	4	# phase transformation id  [-]
		ptdepth         = 	1000	# depth [km]
		npt		=	1	# total phase transformations
		phase_up	=	19 	# phase id above ptdepth
		phase_dn	=	20      # phase id below ptdepth
	<PhaseDepthEnd>

	<PhaseDepthStart>
		ptID 		=	5	# phase transformation id  [-]
		ptdepth         = 	1200	# depth [km]
		npt		=	1	# total phase transformations
		phase_up	=	20 	# phase id above ptdepth
		phase_dn	=	21      # phase id below ptdepth
	<PhaseDepthEnd>

	<PhaseDepthStart>
		ptID 		=	6	# phase transformation id  [-]
		ptdepth         = 	1400	# depth [km]
		npt		=	1	# total phase transformations
		phase_up	=	21 	# phase id above ptdepth
		phase_dn	=	22      # phase id below ptdepth
	<PhaseDepthEnd>

	<PhaseDepthStart>
		ptID 		=	7	# phase transformation id  [-]
		ptdepth         = 	1600	# depth [km]
		npt		=	1	# total phase transformations
		phase_up	=	22 	# phase id above ptdepth
		phase_dn	=	23      # phase id below ptdepth
	<PhaseDepthEnd>

	<PhaseDepthStart>
		ptID 		=	8	# phase transformation id  [-]
		ptdepth         = 	1800	# depth [km]
		npt		=	1	# total phase transformations
		phase_up	=	23 	# phase id above ptdepth
		phase_dn	=	24      # phase id below ptdepth
	<PhaseDepthEnd>

# ----------------------------------------------------
# PETSC OPTIONS (SOLVER)
# ----------------------------------------------------

<PetscOptionsStart>	

	### INFLUX BOUNDARY
	-bvel_face 1			# 1-left 2-right 3-front 4-back boundary 
	-bvel_bot -100
	-bvel_top 0

	-bvel_front 0
	-bvel_back 10			# end of Y-domain if entire boundary - 2560 km
        -bvel_wholebound 1              # 1- outflux on the entire boundary, 0-outflux on partial side of boundary

	-bvel_tstart 0
	-bvel_phase 5

	-bvel_velin 5
	##-bvel_alternate

	-bvel_influxmark
	-bvel_markdir ../Input/MatlabInflux03
	-bvel_xright 6000
	-bvel_length 6000

	-bvel_tseg 3
	-bvel_time 0,0.5,9,100
	-bvel_time_velin 0,7,14,14

	-noslip 1,0,0,0,0,0	# no slip BC face order: left,right,front,back,bottom,top

        -res_log

	# Command-line Options
	-use_fdstag_canonical
	-AddRandomNoiseParticles 0	# Adding noise to the particles (1) or not (0)
	-restart 0
	-shift_press

	### Marker control
	-use_marker_control 
	#-new_mc
	-markers_max 100
	-markers_min 16

	### Advection
	-new_advection
	-advection 0
	-velinterp 2

	### OUTPUT

	# TIME
	-out_pvd 1

	# INTENSIVE VARIABLES
	-out_phase 1  
	-out_density 1
	-out_visc_total 1
	-out_visc_creep 0
	-out_visc_viscoplastic 0

	# SOLUTION
	-out_velocity 1
	-out_pressure 1
	-out_temperature 1 
	-out_dev_stress 1
	-out_j2_dev_stress 1
	-out_strain_rate 1
	-out_j2_strain_rate 1
	-out_plast_strain 0

	# RESIDUALS
	#-out_moment_res 1
	#-out_cont_res 1

	### FREE SURFACE
	-surf_use 1
	-surf_level 0
	-surf_air_phase 1
	-surf_max_angle 45
	-out_surf_pvd 1
	-out_surf_velocity 1
	-out_surf_topography 1
	-out_surf_amplitude 1

	### AVD Phase Viewer
	-out_avd 1 
    	-out_avd_ref 3
    	-out_avd_pvd 1

	### MARKERS
	#-out_markers 1
	#-out_mark_pvd 1

	### BREAKPOINTS
	-secure_breakpoints

	### SOLVER ###

	# SNES (nonlinear) options
	-snes_ksp_ew			# Eisenstat Walker algorithm
	-snes_npicard 10			# 2 picard iterations @ beginning of ever timestep
	-snes_monitor			
	-snes_atol 1e-3
	-snes_rtol 1e-5
	-snes_stol 1e-16
	-snes_max_it 20
	-snes_max_funcs 500000
	-snes_max_linear_solve_fail 10000
	#-snes_type ksponly

	# Newton/picard options
	-snes_PicardSwitchToNewton_rtol 1e-2 		# relative tolerance to switch to Newton (1e-2)
	-snes_NewtonSwitchToPicard_it  	35		# number of Newton iterations after which we switch back to Picard 
	-snes_NewtonSwitchToPicard_rtol 1.1		# relative tolerance compared to first iteration step 

	# Linesearch options
	-snes_linesearch_monitor
	-snes_linesearch_type cp 			#Linesearch type (one of) shell basic l2 bt cp (SNESLineSearchSetType)  [l2 seems to work better with VEP]
	-snes_linesearch_maxstep 1.0			# very important to prevent the code from "blowing up"
	
# Jacobian solver

	-js_ksp_type fgmres
	-js_ksp_max_it 50
	-js_ksp_converged_reason
 	-js_ksp_monitor
	-js_ksp_rtol 1e-5
	-js_ksp_atol 1e-5

# Preconditioner
	-pcmat_type mono
   	-pcmat_pgamma 1e3
  	-jp_type user
   	-jp_pc_type lu
   	-jp_pc_factor_mat_solver_package mumps

	### LATEST additions
	-InitViscosity 1e22 
	-div_tol 1e2

	#-use_quasi_harmonic_viscosity 

	### TEMPERATURE DIFFUSION
	#-act_temp_diff

	-da_refine_y 1

<PetscOptionsEnd>
