% ---------------------------------------------------------------------- #
% Plot00_digitized_conv_data.m
%
% Intended for "ConvIndia" convergence data from published studies
% Author: Adina Pusok, Feb 19, 2019
% ---------------------------------------------------------------------- #
clear
% ---------------------------------------------------------------------- #
% Directories
% ---------------------------------------------------------------------- #
% Input
dir_in = '../../digitized_plots/';

% Output
dir_out = '../../figures/analysis01/';

% ---------------------------------------------------------------------- #
% List of .csv data structure
% ---------------------------------------------------------------------- #
name001 = 'n001_cande_etal_2010_fig11_seir_ck95.csv';
name002 = 'n002_cande_etal_2010_fig11_seir_gos04.csv';
name003 = 'n003_cande_etal_2010_fig13_cir.csv';
name004 = 'n004_cande_etal_2010_fig13_seir.csv';
name005 = 'n005_cande_patriat_2015_fig12_cap_afr.csv';
name006 = 'n006_cande_patriat_2015_fig12_cap_ant.csv';
name007 = 'n007_cande_patriat_2015_fig13_cap_ant_GTS12.csv';
name008 = 'n008_cande_patriat_2015_fig13_cap_ant_CK95.csv';
name009 = 'n009_cande_patriat_2015_fig13_cap_ant_GTS04.csv';

name010 = 'n010_cande_patriat_2015_fig14_cap_afr_GTS04.csv';
name011 = 'n011_cande_patriat_2015_fig14_cap_ant_GTS04.csv';
name012 = 'n012_cande_patriat_2015_fig14_cap_afr_GTS12.csv';
name013 = 'n013_cande_patriat_2015_fig14_cap_ant_GTS12.csv';
name014 = 'n014_cande_patriat_2015_fig15_cap_afr_GTS12.csv';
name015 = 'n015_cande_patriat_2015_fig15_cap_abs_GTS12.csv';
name016 = 'n016_cande_patriat_2015_fig16_cap_ant_rot.csv';
name017 = 'n017_cande_patriat_2015_fig16_cap_ant_ship.csv';
name018 = 'n018_cande_patriat_2015_fig16_cap_ant_rot_corr.csv';
name019 = 'n019_cande_patriat_2015_fig16_cap_ant_ship_corr.csv';
name020 = 'n020_cande_patriat_2015_fig17_cap_afr.csv';

name021 = 'n021_cande_patriat_2015_fig17_cap_afr_eagles_hoang13.csv';
name022 = 'n022_cande_stegman_2011_fig2_ind_ant.csv';
name023 = 'n023_cande_stegman_2011_fig2_ind_afr.csv';
name024 = 'n024_cande_stegman_2011_fig2_ind_afr_syn.csv';
name025 = 'n025_cande_stegman_2011_fig2_ind_ant_syn.csv';
name026 = 'n026_cande_stegman_2011_fig2_ind_ant_ship.csv';
name027 = 'n027_cande_stegman_2011_fig2_ind_afr_ship.csv';
name028 = 'n028_cande_stegman_2011_fig2_ind_afr_syn_corr.csv';
name029 = 'n029_cande_stegman_2011_fig2_ind_ant_syn_corr.csv';
name030 = 'n030_cande_stegman_2011_fig2_ind_ant_ship_corr.csv';

name031 = 'n031_cande_stegman_2011_fig2_ind_afr_ship_corr.csv';
name032 = 'n032_cande_stegman_2011_fig3_ind_afr.csv';
name033 = 'n033_cande_stegman_2011_fig3_ind_afr_ship.csv';
name034 = 'n034_copley_etal_2010_figA1_ind_asia.csv';
name035 = 'n035_copley_etal_2010_figA1_molnar_stock09.csv';
name036 = 'n036_copley_etal_2010_figA1_molnar_stock09_up.csv';
name037 = 'n037_eagles_wibisino_2013_fig5_ind_abs1.csv';
name038 = 'n038_eagles_wibisino_2013_fig5_ind_abs2.csv';
name039 = 'n039_eagles_wibisino_2013_fig5_vanHinsbergen_etal11_ind_abs.csv';
name040 = 'n040_faccenna_etal_2013_fig2_clb07_cir.csv';

name041 = 'n041_faccenna_etal_2013_fig2_ind_abs_model.csv';
name042 = 'n042_gibbons_etal_2015_fig7_ind_eur.csv';
name043 = 'n043_gibbons_etal_2015_fig7_ind_koh.csv';
name044 = 'n044_gibbons_etal_2015_fig7_ind_lha.csv';
name045 = 'n045_gibbons_etal_2015_fig7_ind_ant.csv';
name046 = 'n046_gibbons_etal_2015_fig7_ind_mad.csv';
name047 = 'n047_gibbons_etal_2015_fig7_aus_ind.csv';
name048 = 'n048_vanHinsbergen_etal_2011_fig3_ind_asia_AW.csv';
name049 = 'n049_vanHinsbergen_etal_2011_fig3_ind_asia_AE.csv'; 
name050 = 'n050_vanHinsbergen_etal_2011_fig3_ind_asia_BW.csv';

name051 = 'n051_vanHinsbergen_etal_2011_fig3_ind_asia_BE.csv';
name052 = 'n052_vanHinsbergen_etal_2011_fig3_ind_abs_AW.csv';
name053 = 'n053_vanHinsbergen_etal_2011_fig3_ind_abs_AE.csv';

name054 = 'n054_Hu_etal_2016_fig7.csv';
name055 = 'n055_meng_etal_2012_fig7.csv';

name056 = 'n056_meng_etal_2012_fig7_conv.csv';
name057 = 'n057_nerlich_etal_2016_fig1_gibbons2015.csv';
name058 = 'n058_nerlich_etal_2016_fig1_seton2012.csv';
name059 = 'n059_nerlich_etal_2016_fig1_zahirovic2012.csv';
name060 = 'n060_nerlich_etal_2016_fig1_vanHinsbergen2011.csv';
name061 = 'n061_nerlich_etal_2016_fig1_copley2010.csv';
name062 = 'n062_nerlich_etal_2016_fig1_molnar2009.csv';
name063 = 'n063_nerlich_etal_2016_fig1_lee1995.csv';
name064 = 'n064_nerlich_etal_2016_fig1_patriat1984.csv';
name065 = 'n065_nerlich_etal_2016_fig1_avg.csv';
name066 = 'n066_searle_2015_fig21.csv';
name067 = 'n067_shellnutt_2014_fig3_vanHinsbergen2011.csv';
name068 = 'n068_shellnutt_2014_fig3_copley2010.csv';
name069 = 'n069_shellnutt_2014_fig3_white2012.csv';

name070 = 'n070_vanHinsbergen_2012_fig2.csv';
name071 = 'n071_vanHinsbergen_2018_fig2.csv';
name072 = 'n072_vanHinsbergen_2018_fig10.csv';

name073 = 'n073_White_Lister_2012_fig2_dewey1989.csv';
name074 = 'n074_White_Lister_2012_fig2_molnar2009.csv';
name075 = 'n075_White_Lister_2012_fig2_patriat1984.csv';
name076 = 'n076_White_Lister_2012_fig3_patriat1988.csv';
name077 = 'n077_White_Lister_2012_fig3_molnar1988.csv';
name078 = 'n078_White_Lister_2012_fig4_copley2010.csv';
name079 = 'n079_White_Lister_2012_fig4_copley2010_recalc.csv';
name080 = 'n080_White_Lister_2012_fig8.csv';
name081 = 'n081_Zahirovic_etal_2012_fig2_lee1995.csv';
name082 = 'n082_Zahirovic_etal_2012_fig2_muller2008.csv';
name083 = 'n083_Zahirovic_etal_2012_fig2_molnar2009.csv';
name084 = 'n084_Zahirovic_etal_2012_fig2_vanHinsbergen2011.csv';
name085 = 'n085_Zahirovic_etal_2015_fig4_mhs.csv';
name086 = 'n086_Zahirovic_etal_2015_fig4_vmsr.csv';
name087 = 'n087_Zahirovic_etal_2015_fig4_gmhrf.csv';
name088 = 'n088_Zahirovic_etal_2015_fig4_fhs.csv';

name0 = '';

% ---------------------------------------------------------------------- #
% Error bars files
% ---------------------------------------------------------------------- #
name003_max = 'n003_cande_etal_2010_fig13_cir_max.csv';
name004_max = 'n004_cande_etal_2010_fig13_seir_max.csv';
name005_max = 'n005_cande_patriat_2015_fig12_cap_afr_max.csv';
name006_max = 'n006_cande_patriat_2015_fig12_cap_ant_max.csv';
name022_max = 'n022_cande_stegman_2011_fig2_ind_ant_max.csv';
name023_max = 'n023_cande_stegman_2011_fig2_ind_afr_max.csv';
name042_max = 'n042_gibbons_etal_2015_fig7_ind_eurW.csv';
name044_max = 'n044_gibbons_etal_2015_fig7_ind_lhaW.csv';
name048_max = 'n048_vanHinsbergen_etal_2011_fig3_ind_asia_AW_max.csv';
name049_max = 'n049_vanHinsbergen_etal_2011_fig3_ind_asia_AE_max.csv'; 
name050_max = 'n050_vanHinsbergen_etal_2011_fig3_ind_asia_BW_max.csv';
name051_max = 'n051_vanHinsbergen_etal_2011_fig3_ind_asia_BE_max.csv';
name052_max = 'n052_vanHinsbergen_etal_2011_fig3_ind_abs_AW_max.csv';
name053_max = 'n053_vanHinsbergen_etal_2011_fig3_ind_abs_AE_max.csv';

name003_min = 'n003_cande_etal_2010_fig13_cir_min.csv';
name004_min = 'n004_cande_etal_2010_fig13_seir_min.csv';
name005_min = 'n005_cande_patriat_2015_fig12_cap_afr_min.csv';
name006_min = 'n006_cande_patriat_2015_fig12_cap_ant_min.csv';
name022_min = 'n022_cande_stegman_2011_fig2_ind_ant_min.csv';
name023_min = 'n023_cande_stegman_2011_fig2_ind_afr_min.csv';
name042_min = 'n042_gibbons_etal_2015_fig7_ind_eurE.csv';
name044_min = 'n044_gibbons_etal_2015_fig7_ind_lhaE.csv';
name048_min = 'n048_vanHinsbergen_etal_2011_fig3_ind_asia_AW_min.csv';
name049_min = 'n049_vanHinsbergen_etal_2011_fig3_ind_asia_AE_min.csv'; 
name050_min = 'n050_vanHinsbergen_etal_2011_fig3_ind_asia_BW_min.csv';
name051_min = 'n051_vanHinsbergen_etal_2011_fig3_ind_asia_BE_min.csv';
name052_min = 'n052_vanHinsbergen_etal_2011_fig3_ind_abs_AW_min.csv';
name053_min = 'n053_vanHinsbergen_etal_2011_fig3_ind_abs_AE_min.csv';

% ---------------------------------------------------------------------- #
% Labels
% ---------------------------------------------------------------------- #
lbl001 = 'SEIR spreading (Cap/Ind-Ant) CK95';
lbl002 = 'SEIR spreading (Cap/Ind-Ant) GOS04';
lbl003 = 'CIR spreading (Cap/Ind-Som/Afr) GOS04';
lbl004 = 'SEIR spreading (Cap/Ind-Ant) GOS04';
lbl005 = 'CIR spreading (Cap/Ind-Afr) GTS12';
lbl006 = 'SEIR spreading (Cap/Ind-Ant) GTS12';
lbl007 = 'SEIR spreading (Cap/Ind-Ant) GTS12';
lbl008 = 'SEIR spreading (Cap/Ind-Ant) CK95';
lbl009 = 'SEIR spreading (Cap/Ind-Ant) GTS04';
lbl010 = 'CIR spreading (Cap/Ind-Som/Afr) GTS04';

lbl011 = 'SEIR spreading (Cap/Ind-Ant) GTS04';
lbl012 = 'CIR spreading (Cap/Ind-Som/Afr) GTS12';
lbl013 = 'SEIR spreading (Cap/Ind-Ant) GTS12';
lbl014 = 'CIR spreading (Cap/Ind-Som/Afr) GTS12';
lbl015 = 'Cap/Ind-Abs GTS12';
lbl016 = 'SEIR spreading (Cap/Ind-Ant) GTS12 rot';
lbl017 = 'SEIR spreading (Cap/Ind-Ant) GTS12 ship';
lbl018 = 'SEIR spreading (Cap/Ind-Ant) GTS12 rot corr';
lbl019 = 'SEIR spreading (Cap/Ind-Ant) GTS12 ship corr';
lbl020 = 'CIR spreading (Cap/Ind-Som/Afr) GTS12';

lbl021 = 'CIR spreading (Cap/Ind-Som/Afr) Eagles&Hoang13';
lbl022 = 'SEIR spreading (Ind-Ant) GTS04';
lbl023 = 'CIR spreading (Ind-Afr) GTS04';
lbl024 = 'CIR spreading (Ind-Afr) GTS04 flowline';
lbl025 = 'SEIR spreading (Ind-Ant) GTS04 flowline';
lbl026 = 'SEIR spreading (Ind-Ant) GTS04 ship';
lbl027 = 'CIR spreading (Ind-Afr) GTS04 ship';
lbl028 = 'CIR spreading (Ind-Afr) GTS04 flowline corr';
lbl029 = 'SEIR spreading (Ind-Ant) GTS04 flowline corr';
lbl030 = 'SEIR spreading (Ind-Ant) GTS04 ship corr';

lbl031 = 'CIR spreading (Ind-Afr) GTS04 ship corr';
lbl032 = 'CIR spreading (Ind-Afr) GTS04';
lbl033 = 'CIR spreading (Ind-Afr) GTS04 ship';
lbl034 = 'Plate circuit Ind-Asia';
lbl035 = 'Plate circuit Ind-Asia (Molnar&Stock09)';
lbl036 = 'Plate circuit Ind-Asia (Molnar&Stock09 update)';
lbl037 = 'Ind-Abs Macarene Basin 1';
lbl038 = 'Ind-Abs Macarene Basin 2';
lbl039 = 'Ind-Abs (vanHinsbergen&etal11)';
lbl040 = 'CIR spreading (Ind-Afr) (Conrad&Lithgow-Bertelloni07)';

lbl041 = 'Ind-Abs model';
lbl042 = 'Ind-Eurasia Plate recons model';
lbl043 = 'Ind-Kohistan Plate recons model';
lbl044 = 'Ind-Lhasa Plate recons model';
lbl045 = 'Ind-Ant Plate recons model';
lbl046 = 'Ind-Madagascar/Afr Plate recons model';
lbl047 = 'Ind-Australia Plate recons model';
lbl048 = 'Ind-Asia ReconA W Himalayan syntaxis';
lbl049 = 'Ind-Asia ReconA E Himalayan syntaxis';
lbl050 = 'Ind-Asia ReconB W Himalayan syntaxis';

lbl051 = 'Ind-Asia ReconB E Himalayan syntaxis';
lbl052 = 'Ind-Abs ReconA W Himalayan syntaxis';
lbl053 = 'Ind-Abs ReconA E Himalayan syntaxis';

lbl054 = 'Ind-Eur Paleolatitude';
lbl055 = 'Ind-Eur Paleolatitude';

lbl056 = 'Ind-Eur Paleolatitude (conv)';
lbl057 = 'Ind-Eur (Gibbons et al. 2015, Muller et al 2016)';
lbl058 = 'Ind-Eur (Seton et al. 2012)';
lbl059 = 'Ind-Eur (Zahirovic et al. 2012)';
lbl060 = 'Ind-Eur (van hinsbergen et al. 2011)';
lbl061 = 'Ind-Eur (Copley et al. 2010)';
lbl062 = 'Ind-Eur (Molnar and Stock 2009)';
lbl063 = 'Ind-Eur (Lee and Lawver 1995)';
lbl064 = 'Ind-Eur (Patriat and Achache 1984)';
lbl065 = 'Ind-Eur (Nerlich et al. 2016 - average)';
lbl066 = 'Ind-Eur (Searle 2015)';
lbl067 = 'Ind-Eur (van Hinsbergen et al. 2011)';
lbl068 = 'Ind-Eur (Copley et al. 2010)';
lbl069 = 'Ind-Eur (White and Lister 2012)';

lbl070 = 'Ind-Eur Paleolatitude';
lbl071 = 'Ind-Eur Paleolatitude';
lbl072 = 'Ind-Eur Paleolatitude';

lbl073 = 'Ind-Eur (Dewey et al. 1989)';
lbl074 = 'Ind-Eur (Molnar and Stock 2009)';
lbl075 = 'Ind-Eur (Patriat and Achache 1984)';
lbl076 = 'Ind-Afr (Patriat and Segoufin 1988)';
lbl077 = 'Ind-Afr (Molnar et al. 1988)';
lbl078 = 'Ind-Eur (Copley et al. 2010)';
lbl079 = 'Ind-Eur (recalculated Copley et al. 2010)';
lbl080 = 'Ind-Eur (White and Lister 2012)';
lbl081 = 'Ind-Eur (Lee and Lawver 1995)';
lbl082 = 'Ind-Eur (Muller et al 2008)';
lbl083 = 'Ind-Eur (Molnar and Stock 2009)';
lbl084 = 'Ind-Eur (van Hinsbergen et al. 2011)';
lbl085 = 'Ind-Abs (MHS+TPW-Oneill et al. 2005,Steinberger and Torsvik 2008)';
lbl086 = 'Ind-Abs (VMSR-van der Meer et al. 2010)';
lbl087 = 'Ind-Abs (GMHRF-Doubrovine et al. 2012)';
lbl088 = 'Ind-Abs (FHS-Muller et al. 1993)';

lbl0 = '';

% ---------------------------------------------------------------------- #
% Load .csv data structure
% ---------------------------------------------------------------------- #
data001 = Extract_convergence_values(name001,''         ,''         ,lbl001,0,0,0,dir_in);
data002 = Extract_convergence_values(name002,''         ,''         ,lbl002,0,0,0,dir_in);
data003 = Extract_convergence_values(name003,name003_max,name003_min,lbl003,0,0,0,dir_in);
data004 = Extract_convergence_values(name004,name004_max,name004_min,lbl004,0,0,0,dir_in);
data005 = Extract_convergence_values(name005,name005_max,name005_min,lbl005,0,0,0,dir_in);
data006 = Extract_convergence_values(name006,name006_max,name006_min,lbl006,0,0,0,dir_in);
data007 = Extract_convergence_values(name007,''         ,''         ,lbl007,0,0,0,dir_in);
data008 = Extract_convergence_values(name008,''         ,''         ,lbl008,0,0,0,dir_in);
data009 = Extract_convergence_values(name009,''         ,''         ,lbl009,0,0,0,dir_in);
data010 = Extract_convergence_values(name010,''         ,''         ,lbl010,0,0,0,dir_in);

data011 = Extract_convergence_values(name011,''         ,''         ,lbl011,0,0,0,dir_in);
data012 = Extract_convergence_values(name012,''         ,''         ,lbl012,0,0,0,dir_in);
data013 = Extract_convergence_values(name013,''         ,''         ,lbl013,0,0,0,dir_in);
data014 = Extract_convergence_values(name014,''         ,''         ,lbl014,0,0,0,dir_in);
data015 = Extract_convergence_values(name015,''         ,''         ,lbl015,0,0,0,dir_in);
data016 = Extract_convergence_values(name016,''         ,''         ,lbl016,0,0,0,dir_in);
data017 = Extract_convergence_values(name017,''         ,''         ,lbl017,0,0,0,dir_in);
data018 = Extract_convergence_values(name018,''         ,''         ,lbl018,0,0,0,dir_in);
data019 = Extract_convergence_values(name019,''         ,''         ,lbl019,0,0,0,dir_in);
data020 = Extract_convergence_values(name020,''         ,''         ,lbl020,0,0,0,dir_in);

data021 = Extract_convergence_values(name021,''         ,''         ,lbl021,0,0,0,dir_in);
data022 = Extract_convergence_values(name022,name022_max,name022_min,lbl022,0,0,0,dir_in);
data023 = Extract_convergence_values(name023,name023_max,name023_min,lbl023,0,0,0,dir_in);
data024 = Extract_convergence_values(name024,''         ,''         ,lbl024,0,0,0,dir_in);
data025 = Extract_convergence_values(name025,''         ,''         ,lbl025,0,0,0,dir_in);
data026 = Extract_convergence_values(name026,''         ,''         ,lbl026,0,0,0,dir_in);
data027 = Extract_convergence_values(name027,''         ,''         ,lbl027,0,0,0,dir_in);
data028 = Extract_convergence_values(name028,''         ,''         ,lbl028,0,0,0,dir_in);
data029 = Extract_convergence_values(name029,''         ,''         ,lbl029,0,0,0,dir_in);
data030 = Extract_convergence_values(name030,''         ,''         ,lbl030,0,0,0,dir_in);

data031 = Extract_convergence_values(name031,''         ,''         ,lbl031,0,0,0,dir_in);
data032 = Extract_convergence_values(name032,''         ,''         ,lbl032,0,0,0,dir_in);
data033 = Extract_convergence_values(name033,''         ,''         ,lbl033,0,0,0,dir_in);
data034 = Extract_convergence_values(name034,''         ,''         ,lbl034,0,0,0,dir_in);
data035 = Extract_convergence_values(name035,''         ,''         ,lbl035,0,0,0,dir_in);
data036 = Extract_convergence_values(name036,''         ,''         ,lbl036,0,0,0,dir_in);
data037 = Extract_convergence_values(name037,''         ,''         ,lbl037,0,0,0,dir_in);
data038 = Extract_convergence_values(name038,''         ,''         ,lbl038,0,0,0,dir_in);
data039 = Extract_convergence_values(name039,''         ,''         ,lbl039,0,0,0,dir_in);
data040 = Extract_convergence_values(name040,''         ,''         ,lbl040,1,1,0,dir_in);

data041 = Extract_convergence_values(name041,''         ,''         ,lbl041,1,0,0,dir_in);
data042 = Extract_convergence_values(name042,name042_max,name042_min,lbl042,1,0,1,dir_in);
data043 = Extract_convergence_values(name043,''         ,''         ,lbl043,1,0,0,dir_in);
data044 = Extract_convergence_values(name044,name044_max,name044_min,lbl044,1,0,1,dir_in);
data045 = Extract_convergence_values(name045,''         ,''         ,lbl045,0,0,0,dir_in);
data046 = Extract_convergence_values(name046,''         ,''         ,lbl046,0,0,0,dir_in);
data047 = Extract_convergence_values(name047,''         ,''         ,lbl047,0,0,0,dir_in);
data048 = Extract_convergence_values(name048,name048_max,name048_min,lbl048,0,0,0,dir_in);
data049 = Extract_convergence_values(name049,name049_max,name049_min,lbl049,0,0,0,dir_in);
data050 = Extract_convergence_values(name050,name050_max,name050_min,lbl050,0,0,0,dir_in);

data051 = Extract_convergence_values(name051,name051_max,name051_min,lbl051,0,0,0,dir_in);
data052 = Extract_convergence_values(name052,name052_max,name052_min,lbl052,0,0,0,dir_in);
data053 = Extract_convergence_values(name053,name053_max,name053_min,lbl053,0,0,0,dir_in);

data054 = Extract_convergence_values_paleolat(name054,lbl054,dir_in);
data055 = Extract_convergence_values_paleolat(name055,lbl055,dir_in);

data056 = Extract_convergence_values(name056,''         ,''         ,lbl056,1,0,0,dir_in);
data057 = Extract_convergence_values(name057,''         ,''         ,lbl057,1,0,0,dir_in);
data058 = Extract_convergence_values(name058,''         ,''         ,lbl058,1,0,0,dir_in);
data059 = Extract_convergence_values(name059,''         ,''         ,lbl059,1,0,0,dir_in);
data060 = Extract_convergence_values(name060,''         ,''         ,lbl060,1,0,0,dir_in);
data061 = Extract_convergence_values(name061,''         ,''         ,lbl061,1,0,0,dir_in);
data062 = Extract_convergence_values(name062,''         ,''         ,lbl062,1,0,0,dir_in);
data063 = Extract_convergence_values(name063,''         ,''         ,lbl063,1,0,0,dir_in);
data064 = Extract_convergence_values(name064,''         ,''         ,lbl064,1,0,0,dir_in);
data065 = Extract_convergence_values(name065,''         ,''         ,lbl065,1,0,0,dir_in);
data066 = Extract_convergence_values(name066,''         ,''         ,lbl066,0,0,0,dir_in);
data067 = Extract_convergence_values(name067,''         ,''         ,lbl067,0,0,0,dir_in);
data068 = Extract_convergence_values(name068,''         ,''         ,lbl068,0,0,0,dir_in);
data069 = Extract_convergence_values(name069,''         ,''         ,lbl069,0,0,0,dir_in);

data070 = Extract_convergence_values_paleolat(name070,lbl070,dir_in);
data071 = Extract_convergence_values_paleolat(name071,lbl071,dir_in);
data072 = Extract_convergence_values_paleolat(name072,lbl072,dir_in);

data073 = Extract_convergence_values(name073,''         ,''         ,lbl073,0,0,0,dir_in);
data074 = Extract_convergence_values(name074,''         ,''         ,lbl074,0,0,0,dir_in);
data075 = Extract_convergence_values(name075,''         ,''         ,lbl075,0,0,0,dir_in);
data076 = Extract_convergence_values(name076,''         ,''         ,lbl076,0,0,0,dir_in);
data077 = Extract_convergence_values(name077,''         ,''         ,lbl077,0,0,0,dir_in);
data078 = Extract_convergence_values(name078,''         ,''         ,lbl078,0,0,0,dir_in);
data079 = Extract_convergence_values(name079,''         ,''         ,lbl079,0,0,0,dir_in);
data080 = Extract_convergence_values(name080,''         ,''         ,lbl080,0,0,0,dir_in);
data081 = Extract_convergence_values(name081,''         ,''         ,lbl081,0,0,0,dir_in);
data082 = Extract_convergence_values(name082,''         ,''         ,lbl082,0,0,0,dir_in);
data083 = Extract_convergence_values(name083,''         ,''         ,lbl083,0,0,0,dir_in);
data084 = Extract_convergence_values(name084,''         ,''         ,lbl084,0,0,0,dir_in);
data085 = Extract_convergence_values(name085,''         ,''         ,lbl085,1,0,0,dir_in);
data086 = Extract_convergence_values(name086,''         ,''         ,lbl086,1,0,0,dir_in);
data087 = Extract_convergence_values(name087,''         ,''         ,lbl087,1,0,0,dir_in);
data088 = Extract_convergence_values(name088,''         ,''         ,lbl088,1,0,0,dir_in);

% ---------------------------------------------------------------------- #
% Plot all data
% ---------------------------------------------------------------------- #
Plot01_individual_age_rate(data001,dir_out);
Plot01_individual_age_rate(data002,dir_out);
Plot01_individual_age_rate(data003,dir_out);
Plot01_individual_age_rate(data004,dir_out);
Plot01_individual_age_rate(data005,dir_out);
Plot01_individual_age_rate(data006,dir_out);
Plot01_individual_age_rate(data007,dir_out);
Plot01_individual_age_rate(data008,dir_out);
Plot01_individual_age_rate(data009,dir_out);
Plot01_individual_age_rate(data010,dir_out);

Plot01_individual_age_rate(data011,dir_out);
Plot01_individual_age_rate(data012,dir_out);
Plot01_individual_age_rate(data013,dir_out);
Plot01_individual_age_rate(data014,dir_out);
Plot01_individual_age_rate(data015,dir_out);
Plot01_individual_age_rate(data016,dir_out);
Plot01_individual_age_rate(data017,dir_out);
Plot01_individual_age_rate(data018,dir_out);
Plot01_individual_age_rate(data019,dir_out);
Plot01_individual_age_rate(data020,dir_out);

Plot01_individual_age_rate(data021,dir_out);
Plot01_individual_age_rate(data022,dir_out);
Plot01_individual_age_rate(data023,dir_out);
Plot01_individual_age_rate(data024,dir_out);
Plot01_individual_age_rate(data025,dir_out);
Plot01_individual_age_rate(data026,dir_out);
Plot01_individual_age_rate(data027,dir_out);
Plot01_individual_age_rate(data028,dir_out);
Plot01_individual_age_rate(data029,dir_out);
Plot01_individual_age_rate(data030,dir_out);

Plot01_individual_age_rate(data031,dir_out);
Plot01_individual_age_rate(data032,dir_out);
Plot01_individual_age_rate(data033,dir_out);
Plot01_individual_age_rate(data034,dir_out);
Plot01_individual_age_rate(data035,dir_out);
Plot01_individual_age_rate(data036,dir_out);
Plot01_individual_age_rate(data037,dir_out);
Plot01_individual_age_rate(data038,dir_out);
Plot01_individual_age_rate(data039,dir_out);
Plot01_individual_age_rate(data040,dir_out);

Plot01_individual_age_rate(data041,dir_out);
Plot01_individual_age_rate(data042,dir_out);
Plot01_individual_age_rate(data043,dir_out);
Plot01_individual_age_rate(data044,dir_out);
Plot01_individual_age_rate(data045,dir_out);
Plot01_individual_age_rate(data046,dir_out);
Plot01_individual_age_rate(data047,dir_out);
Plot01_individual_age_rate(data048,dir_out);
Plot01_individual_age_rate(data049,dir_out);
Plot01_individual_age_rate(data050,dir_out);

Plot01_individual_age_rate(data051,dir_out);
Plot01_individual_age_rate(data052,dir_out);
Plot01_individual_age_rate(data053,dir_out);

Plot01_individual_age_rate(data054,dir_out);
Plot01_individual_age_rate(data055,dir_out);

Plot01_individual_age_rate(data056,dir_out);
Plot01_individual_age_rate(data057,dir_out);
Plot01_individual_age_rate(data058,dir_out);
Plot01_individual_age_rate(data059,dir_out);
Plot01_individual_age_rate(data060,dir_out);
Plot01_individual_age_rate(data061,dir_out);
Plot01_individual_age_rate(data062,dir_out);
Plot01_individual_age_rate(data063,dir_out);
Plot01_individual_age_rate(data064,dir_out);
Plot01_individual_age_rate(data065,dir_out);
Plot01_individual_age_rate(data066,dir_out);
Plot01_individual_age_rate(data067,dir_out);
Plot01_individual_age_rate(data068,dir_out);
Plot01_individual_age_rate(data069,dir_out);

Plot01_individual_age_rate(data070,dir_out);
Plot01_individual_age_rate(data071,dir_out);
Plot01_individual_age_rate(data072,dir_out);

Plot01_individual_age_rate(data073,dir_out);
Plot01_individual_age_rate(data074,dir_out);
Plot01_individual_age_rate(data075,dir_out);
Plot01_individual_age_rate(data076,dir_out);
Plot01_individual_age_rate(data077,dir_out);
Plot01_individual_age_rate(data078,dir_out);
Plot01_individual_age_rate(data079,dir_out);
Plot01_individual_age_rate(data080,dir_out);
Plot01_individual_age_rate(data081,dir_out);
Plot01_individual_age_rate(data082,dir_out);
Plot01_individual_age_rate(data083,dir_out);
Plot01_individual_age_rate(data084,dir_out);
Plot01_individual_age_rate(data085,dir_out);
Plot01_individual_age_rate(data086,dir_out);
Plot01_individual_age_rate(data087,dir_out);
Plot01_individual_age_rate(data088,dir_out);


