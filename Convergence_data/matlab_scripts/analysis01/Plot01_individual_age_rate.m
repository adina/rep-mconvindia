function [] = Plot01_individual_age_rate(DataX,dir_out)
% Plot01 - plot age (Myr) vs rate (mm/yr)

% Prepare data
age  = DataX.age;
u    = DataX.u0;
er   = DataX.err;
lbl  = DataX.lbl;
name = DataX.name;

% Prepare Figure
figure(1); clf;
hold on
grid on
box on

font_size  = 14;
size_color = 3;

% Axes
xmin = 0; xmax = 90;    % Age (Myr)
ymin = 0; ymax = 250;   % Rate (mm/yr)

% Colors
cgrey = [0.7 0.7 0.7];
corange = [1.0 128/255 0.0]; % #FF8000
cred    = [204/255 0.0 0.0]; % #CC0000
cyellow = [1.0 215/255 0.0]; % #FFD700
cgreen  = [60/255 179/255 113/255]; % #3CB371
cblue   = [0.0 0.0 205/255]; % #0000CD

% Define delimiters
xstage0 = 90;
xstage1 = 72; % plume
xstage2 = 65; % max plume
xstage3 = 62; % drop, change in force balance
xstage4 = 60; % double subd
xstage5 = 50; % arc-collision/end of double subd
xstage6 = 45; % cont-collision
xstage7 =  0; % present-day

% Plot error bars

% Plot data delimiters (del00)
%h01 = area([xstage1 xstage0 xstage0 xstage1],[ymin ymin ymax ymax],'FaceColor',cgrey,'FaceAlpha',0.5,'LineStyle','none'); % plume arrival
%h02 = area([xstage3 xstage2 xstage2 xstage3],[ymin ymin ymax ymax],'FaceColor',cgrey,'FaceAlpha',0.5,'LineStyle','none'); % max plume
%h03 = area([xstage5 xstage4 xstage4 xstage5],[ymin ymin ymax ymax],'FaceColor',cgrey,'FaceAlpha',0.5,'LineStyle','none'); % double subd
%h04 = area([xstage7 xstage6 xstage6 xstage7],[ymin ymin ymax ymax],'FaceColor',cgrey,'FaceAlpha',0.5,'LineStyle','none'); % double subd

% Plot data delimiters (del01)
%h01 = area([xstage2 xstage1 xstage1 xstage2],[ymin ymin ymax ymax],'FaceColor',cgrey,'FaceAlpha',0.5,'LineStyle','none'); % plume arrival
%h02 = area([xstage4 xstage3 xstage3 xstage4],[ymin ymin ymax ymax],'FaceColor',cgrey,'FaceAlpha',0.5,'LineStyle','none'); % max plume
%h03 = area([xstage6 xstage5 xstage5 xstage6],[ymin ymin ymax ymax],'FaceColor',cgrey,'FaceAlpha',0.5,'LineStyle','none'); % double subd

% Plot data delimiters (del01)
h01 = fill([xstage2 xstage1 xstage1 xstage2],[ymin ymin ymax ymax],corange,'FaceAlpha',0.4,'LineStyle','none'); % plume arrival
h02 = fill([xstage3 xstage2 xstage2 xstage3],[ymin ymin ymax ymax],cred,'FaceAlpha',0.4,'LineStyle','none'); % max plume
h03 = fill([xstage4 xstage3 xstage3 xstage4],[ymin ymin ymax ymax],cyellow,'FaceAlpha',0.4,'LineStyle','none'); % plume drop
h04 = fill([xstage5 xstage4 xstage4 xstage5],[ymin ymin ymax ymax],cgreen,'FaceAlpha',0.4,'LineStyle','none'); % double subd
h05 = fill([xstage6 xstage5 xstage5 xstage6],[ymin ymin ymax ymax],cblue,'FaceAlpha',0.4,'LineStyle','none'); % arc-cont collision

% Plot error bars
if (er.agemin)
    h10 = patch([er.agemin; fliplr(er.agemax')'],[er.umin; fliplr(er.umax')'],'g');
end

% Plot data
h00 = plot(age,u,'k','LineWidth',2);

% Axes and labels
set(gca,'xdir','reverse');
axis([xmin xmax ymin ymax]);

% Labels
xlabel('Age (Myr)','FontSize',font_size);
ylabel('Rate (mm/yr)','FontSize',font_size);
set(gca,'FontSize',font_size);

h = legend(h00,{lbl},'Location','NorthEast','FontSize',font_size-3,'Interpreter', 'none');
title(name(1:end-4), 'FontSize',font_size-2,'Interpreter', 'none');

% Create figure
set(gcf, 'Position', [0, 0, 500, 400]);
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 5.00 4.00])

% Create file name 
fname = [dir_out name(1:end-4) '.png'];

% Save name
print(figure(1),fname,'-dpng');

end