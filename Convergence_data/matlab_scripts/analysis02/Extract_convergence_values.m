function [DataX] = Extract_convergence_values(name,name_max,name_min,lbl,units,half,EW,dir_in)
% Parameters:
%   name     - name of file
%   name_max - name of file with max errors
%   name_min - name of file with min errors
%   lbl      - plot label
%   units    - units of data: 0-mm/yr, 1-cm/yr
%   half     - rate type: 0-full spreading, 1-half-spreading
%   EW       - some data only have motion of E/W syntaxis 0-no, 1-yes
%   dir_in   - directory of data

% ---------------------------------------------------------------------- #
% Load errors if the case
% ---------------------------------------------------------------------- #
if (name_max)
    % Load data
    m_max = importdata([dir_in name_max]);
    
    % Age (Ma)
    err0.agemax = m_max(:,1);
    
    % Spreading rate
    if (units == 0)
        err0.umax   = m_max(:,2); % [mm/yr]
    else
        err0.umax   = 10.*m_max(:,2); % [cm/yr]
    end
    
    % Check if half-spreading rate
    if (half==1)
        err0.umax = 2.*err0.umax;
    end
    
else
    % Initialize structures
    err0.agemax = [];
    err0.umax   = [];
end

if (name_min)
    % Spreading rate
    m_min = importdata([dir_in name_min]);
    
    % Age (Ma)
    err0.agemin = m_min(:,1);
    
    % Spreading rate
    if (units == 0)
        err0.umin   = m_min(:,2); % [mm/yr]
    else
        err0.umin   = 10.*m_min(:,2); % [cm/yr]
    end
    
    % Check if half-spreading rate
    if (half==1)
        err0.umin = 2.*err0.umin;
    end
    
else
    % Initialize structures
    err0.agemin = [];
    err0.umin   = [];
end

% ---------------------------------------------------------------------- #
% Load main .csv data
% ---------------------------------------------------------------------- #
if (EW == 0) % main data is provided
    % Load data
    m   = importdata([dir_in name]);
    
    % Age (Ma)
    age = m(:,1);
    
    % Spreading rate
    if (units == 0)
        u0  = m(:,2); % [mm/yr]
    else
        u0  = 10.*m(:,2); % [cm/yr]
    end
    
    % Check if half-spreading rate
    if (half==1)
        u0 = 2.*u0;
    end
else
    age = [];
    u0 =  [];
    
%     % Calculate data from errors (EW-Gibbons)
%     % Need to interpolate data
%     n = max(length(err0.agemax),length(err0.agemin));
%     
%     if n==length(err0.agemax)
%         % Age (Ma)
%         age = err0.agemax;
%         
%         % Interp the other
%         uinterp = interp1(err0.agemin,err0.umin,age);
%         
%         % Spreading rate
%         u0  = (err0.umax+uinterp)./2;
%         
%     else
%         % Age (Ma)
%         age = err0.agemin;
%         
%         % Interp the other
%         uinterp = interp1(err0.agemax,err0.umax,age);
%         
%         % Spreading rate
%         u0  = (err0.umin+uinterp)./2;
%     end
end

% ---------------------------------------------------------------------- #
% Save structure
% ---------------------------------------------------------------------- #
DataX.name       = name;
DataX.name_max   = name_max;
DataX.name_min   = name_min;
DataX.lbl        = lbl;
DataX.age        = age;
DataX.u0         = u0;
DataX.err.agemax = err0.agemax;
DataX.err.agemin = err0.agemin;
DataX.err.umax   = err0.umax;
DataX.err.umin   = err0.umin;

end