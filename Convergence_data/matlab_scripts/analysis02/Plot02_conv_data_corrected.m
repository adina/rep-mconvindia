% ---------------------------------------------------------------------- #
% Plot02_conv_data.m
%
% Intended for "ConvIndia" convergence data from published studies
% Author: Adina Pusok, Apr 10, 2019
% ---------------------------------------------------------------------- #
clear
% ---------------------------------------------------------------------- #
% Directories
% ---------------------------------------------------------------------- #
% Input
dir_in     = '../../digitized_plots/';
dir_in_num = './numdata/';

% Output
%dir_out = '../../figures/analysis02/'; % previous
dir_out = './';

% ---------------------------------------------------------------------- #
% List of .csv data structure
% ---------------------------------------------------------------------- #
name002 = 'n002_cande_etal_2010_fig11_seir_gos04.csv';
name007 = 'n007_cande_patriat_2015_fig13_cap_ant_GTS12.csv';
name023 = 'n023_cande_stegman_2011_fig2_ind_afr.csv';
name012 = 'n012_cande_patriat_2015_fig14_cap_afr_GTS12.csv';

% ---------------------------------------------------------------------- #
% Numerical data
% ---------------------------------------------------------------------- #
sname05 = 'ConvIndia05';
sname10 = 'ConvIndia10';
sname15 = 'ConvIndia15';
sname20 = 'ConvIndia20';
sname25 = 'ConvIndia25';
sname30 = 'ConvIndia30';
sname35 = 'ConvIndia35';
sname40 = 'ConvIndia40';

% ---------------------------------------------------------------------- #
% Labels
% ---------------------------------------------------------------------- #
lbl002 = 'SEIR (Ind-Ant, GTS04)';
lbl007 = 'SEIR (Ind-Ant, GTS12)';
lbl023 = 'CIR (Ind-Afr, GTS04)';
lbl012 = 'CIR (Ind-Afr, GTS12)';

slbl005 = 'Model ConvIndia05';
slbl010 = 'Model ConvIndia10';
slbl015 = 'Model ConvIndia15';
slbl020 = 'Model ConvIndia20';
slbl025 = 'Model ConvIndia25';
slbl030 = 'Model ConvIndia30';
slbl035 = 'Model ConvIndia35';
slbl040 = 'Model ConvIndia40';

% ---------------------------------------------------------------------- #
% Load .csv data structure
% ---------------------------------------------------------------------- #
data002 = Extract_convergence_values(name002,'','',lbl002,0,0,0,dir_in);
data007 = Extract_convergence_values(name007,'','',lbl007,0,0,0,dir_in);
data012 = Extract_convergence_values(name012,'','',lbl012,0,0,0,dir_in);
data023 = Extract_convergence_values(name023,'','',lbl023,0,0,0,dir_in);

% ---------------------------------------------------------------------- #
% Load numerical data
% ---------------------------------------------------------------------- #
load([dir_in_num sname05 '.mat']); S05 = U0; clearvars U0;
load([dir_in_num sname10 '.mat']); S10 = U0; clearvars U0;
load([dir_in_num sname15 '.mat']); S15 = U0; clearvars U0;
load([dir_in_num sname20 '.mat']); S20 = U0; clearvars U0;
load([dir_in_num sname25 '.mat']); S25 = U0; clearvars U0;
load([dir_in_num sname30 '.mat']); S30 = U0; clearvars U0;
load([dir_in_num sname35 '.mat']); S35 = U0; clearvars U0;
load([dir_in_num sname40 '.mat']); S40 = U0; clearvars U0;

% Load analysis data (containing max single velocity, each item represents 1 sim)
load([dir_in_num 'An.mat']);

% ---------------------------------------------------------------------- #
% Calculate numerical data
% ---------------------------------------------------------------------- #
% Create timesteps for u0
timec05 = (S05.time(2:end)+S05.time(1:end-1))./2;
timec10 = (S10.time(2:end)+S10.time(1:end-1))./2;
timec15 = (S15.time(2:end)+S15.time(1:end-1))./2;
timec20 = (S20.time(2:end)+S20.time(1:end-1))./2;
timec25 = (S25.time(2:end)+S25.time(1:end-1))./2;
timec30 = (S30.time(2:end)+S30.time(1:end-1))./2;
timec35 = (S35.time(2:end)+S35.time(1:end-1))./2;
timec40 = (S40.time(2:end)+S40.time(1:end-1))./2;

% ---------------------------------------------------------------------- #
% Plot data
% ---------------------------------------------------------------------- #
% Iterate between influxBC06 models
for i=7:7
    
    % Prepare data
    switch i
        case 1
            S     = S05;
            timec = timec05;
            sname = sname05;
            lbl   = slbl005;
            u0sin = An.u0_maxsingle(5);
            
        case 2
            S     = S10;
            timec = timec10;
            sname = sname10;
            lbl   = slbl010;
            u0sin = An.u0_maxsingle(10);
        case 3
            S     = S15;
            timec = timec15;
            sname = sname15;
            lbl   = slbl015;
            u0sin = An.u0_maxsingle(15);
        case 4
            S     = S20;
            timec = timec20;
            sname = sname20;
            lbl   = slbl020;
            u0sin = An.u0_maxsingle(20);
        case 5
            S     = S25;
            timec = timec25;
            sname = sname25;
            lbl   = slbl025;
            u0sin = An.u0_maxsingle(25);
        case 6
            S     = S30;
            timec = timec30;
            sname = sname30;
            lbl   = slbl030;
            u0sin = An.u0_maxsingle(30);
        case 7
            S     = S35;
            timec = timec35;
            sname = sname35;
            lbl   = slbl035;
            u0sin = An.u0_maxsingle(35);
        case 8
            S     = S40;
            timec = timec40;
            sname = sname40;
            lbl   = slbl040;
            u0sin = An.u0_maxsingle(40);
    end
    
    % ---------------------------------------------------------------------- #
    % FIGURE 1 - CONVERGENCE
    % ---------------------------------------------------------------------- #
    % Prepare Figure
    figure(1); clf;
    hold on
    grid on
    box on
    
    font_size  = 14;
    size_color = 3;
    
    % Axes
    xmin = 0; xmax = 90;    % Age (Myr)
    ymin = 0; ymax = 200;   % Rate (mm/yr)
    
    % Colors
    cgrey1  = [0.5 0.5 0.5];
    corange = [1.0 128/255 0.0]; % #FF8000
    cred    = [204/255 0.0 0.0]; % #CC0000
    cyellow = [1.0 215/255 0.0]; % #FFD700
    cgreen  = [60/255 179/255 113/255]; % #3CB371
    cblue   = [0.0 0.0 205/255]; % #0000CD
    
    % Versions on color
    color_aqua1 = [0.00 0.80 0.80]; % aqua1
    cgreen2     = [0.00 128/255 0.00]; % #008000
    cblue2      = [30/255 144/255 255/255];
    
    cmix = cblue2;
    
    % Define delimiters
    xstage0 = 90;
    xstage1 = 72; % plume
    xstage2 = 65; % max plume
    xstage3 = 62; % drop, change in force balance
    xstage4 = 60; % double subd
    xstage5 = 50; % arc-collision/end of double subd
    xstage6 = 45; % cont-collision
    xstage7 =  0; % present-day
    
    % Plot data delimiters (del01)
    h21 = fill([xstage2 xstage1 xstage1 xstage2],[ymin ymin ymax ymax],corange,'FaceAlpha',0.4,'LineStyle','none'); % plume arrival
    h22 = fill([xstage3 xstage2 xstage2 xstage3],[ymin ymin ymax ymax],cred,'FaceAlpha',0.4,'LineStyle','none'); % max plume
    h23 = fill([xstage4 xstage3 xstage3 xstage4],[ymin ymin ymax ymax],cyellow,'FaceAlpha',0.4,'LineStyle','none'); % plume drop
    h24 = fill([xstage5 xstage4 xstage4 xstage5],[ymin ymin ymax ymax],cgreen,'FaceAlpha',0.4,'LineStyle','none'); % double subd
    h25 = fill([xstage6 xstage5 xstage5 xstage6],[ymin ymin ymax ymax],cblue,'FaceAlpha',0.4,'LineStyle','none'); % arc-cont collision
    
    % Plot convergence data
    h00 = plot(data002.age,data002.u0,'-','Color',cgrey1,'LineWidth',2);
    h01 = plot(data007.age,data007.u0,':','Color',cgrey1,'LineWidth',2);
    h02 = plot(data023.age,data023.u0,'k-','LineWidth',2);
    h03 = plot(data012.age,data012.u0,'k:','LineWidth',2);
    
    % Plot numerical data
    tstart = 71;
    ind    = find(S.u0.*10>20); % plot values above 40 mm/yr
    h10 = plot(tstart-timec(ind),S.u0(ind).*10,'-','Color',cmix,'LineWidth',3);
    
    % Axes and labels
    set(gca,'xdir','reverse');
    axis([xmin xmax ymin ymax]);
    
    % Labels
    xlabel('Age (Myr)','FontSize',font_size+2);
    ylabel('Spreading/convergence rate (mm/yr)','FontSize',font_size+1);
    set(gca,'FontSize',font_size);
    
    %h = legend([h00 h01 h02 h03 h10],{lbl002,lbl007,lbl023,lbl012,lbl},'Location','NorthEast','FontSize',font_size-3,'Interpreter', 'none');
    %title(name(1:end-4), 'FontSize',font_size-2,'Interpreter', 'none');
    
    % Create figure
    set(gcf, 'Position', [0, 0, 700, 500]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 7.00 5.00])
    
    % Create file name
    fname = [dir_out 'fig02_convergence_data_' sname '.png'];
    
    % Save name
    print(figure(1),fname,'-dpng');
    
    % ---------------------------------------------------------------------- #
    % FIGURE 2 - SPEED-UP
    % ---------------------------------------------------------------------- #
    % Prepare Figure
    figure(2); clf;
    hold on
    grid on
    box on
    
    font_size  = 14;
    size_color = 3;
    
    % Axes
    xmin = 0; xmax = 90;    % Age (Myr)
    ymin = 0; ymax =  4;    % Speed-up
    
    % Colors
    cgrey1  = [0.5 0.5 0.5];
    corange = [1.0 128/255 0.0]; % #FF8000
    cred    = [204/255 0.0 0.0]; % #CC0000
    cyellow = [1.0 215/255 0.0]; % #FFD700
    cgreen  = [60/255 179/255 113/255]; % #3CB371
    cblue   = [0.0 0.0 205/255]; % #0000CD
    
    % Define delimiters
    xstage0 = 90;
    xstage1 = 72; % plume
    xstage2 = 65; % max plume
    xstage3 = 62; % drop, change in force balance
    xstage4 = 60; % double subd
    xstage5 = 50; % arc-collision/end of double subd
    xstage6 = 45; % cont-collision
    xstage7 =  0; % present-day
    
    % Plot data delimiters (del01)
    h21 = fill([xstage2 xstage1 xstage1 xstage2],[ymin ymin ymax ymax],corange,'FaceAlpha',0.4,'LineStyle','none'); % plume arrival
    h22 = fill([xstage3 xstage2 xstage2 xstage3],[ymin ymin ymax ymax],cred,'FaceAlpha',0.4,'LineStyle','none'); % max plume
    h23 = fill([xstage4 xstage3 xstage3 xstage4],[ymin ymin ymax ymax],cyellow,'FaceAlpha',0.4,'LineStyle','none'); % plume drop
    h24 = fill([xstage5 xstage4 xstage4 xstage5],[ymin ymin ymax ymax],cgreen,'FaceAlpha',0.4,'LineStyle','none'); % double subd
    h25 = fill([xstage6 xstage5 xstage5 xstage6],[ymin ymin ymax ymax],cblue,'FaceAlpha',0.4,'LineStyle','none'); % arc-cont collision
    
    u0seir = 70; %data002.u0(1);
    u0cir  = 70; %data023.u0(1);
    
    % Plot speed-up data
    h00 = plot(data002.age,data002.u0./u0seir,'-','Color',cgrey1,'LineWidth',2);
    h01 = plot(data007.age,data007.u0./u0seir,':','Color',cgrey1,'LineWidth',2);
    h02 = plot(data023.age,data023.u0./u0cir,'k-','LineWidth',2);
    h03 = plot(data012.age,data012.u0./u0cir,'k:','LineWidth',2);
    
    % Plot speed-up numerical data
    tstart = 71;
    h10 = plot(tstart-timec(ind),S.u0(ind)./u0sin,'-','Color',cmix,'LineWidth',3);
    
    % Axes and labels
    set(gca,'xdir','reverse');
    axis([xmin xmax ymin ymax]);
    
    % Labels
    xlabel('Age (Myr)','FontSize',font_size+2);
    ylabel('Speed-up (\times V^{Single subd})','FontSize',font_size+2);
    set(gca,'FontSize',font_size);
    
    h = legend([h00 h01 h02 h03 h10],{lbl002,lbl007,lbl023,lbl012,lbl},'Location','NorthEast','FontSize',font_size-3,'Interpreter', 'none');
    %title(name(1:end-4), 'FontSize',font_size-2,'Interpreter', 'none');
    
    % Create figure
    set(gcf, 'Position', [0, 0, 700, 500]);
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 7.00 5.00])
    
    % Create file name
    fname = [dir_out 'fig02_speedup_' sname '.png'];
    
    % Save name
    print(figure(2),fname,'-dpng');
end

