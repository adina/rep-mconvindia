% ---------------------------------------------------------------------- #
% Plot13_cir_3p_gts04_syn.m
%
% Intended for "ConvIndia" convergence data from published studies
% Author: Adina Pusok, Mar 3, 2019
% ---------------------------------------------------------------------- #
clear
% ---------------------------------------------------------------------- #
% Directories
% ---------------------------------------------------------------------- #
% Input
dir_in = '../../digitized_plots/';

% Output
dir_out = '../../figures/analysis03/';

% Plot filename
name_out = 'Plot13_cir_3p_gts04_syn.png';

% ---------------------------------------------------------------------- #
% List of .csv data structure
% ---------------------------------------------------------------------- #
name01 = 'n024_cande_stegman_2011_fig2_ind_afr_syn.csv';
name02 = 'n028_cande_stegman_2011_fig2_ind_afr_syn_corr.csv';

%name024 = 'n024_cande_stegman_2011_fig2_ind_afr_syn.csv';
%name028 = 'n028_cande_stegman_2011_fig2_ind_afr_syn_corr.csv';

% ---------------------------------------------------------------------- #
% Error bars files
% ---------------------------------------------------------------------- #
%name_max = 'n006_cande_patriat_2015_fig12_cap_ant_max.csv';
%name_min = 'n006_cande_patriat_2015_fig12_cap_ant_min.csv';

% ---------------------------------------------------------------------- #
% Labels
% ---------------------------------------------------------------------- #
lbl01 = 'CIR, 3P, GTS04, flowline';
lbl02 = 'CIR, 3P, GTS04, flowline corr';
ttl = 'Cande and Stegman (2011)';

% ---------------------------------------------------------------------- #
% Load .csv data structure
% ---------------------------------------------------------------------- #
data01 = Extract_convergence_values(name01,'','',lbl01,0,0,0,dir_in);
data02 = Extract_convergence_values(name02,'','',lbl02,0,0,0,dir_in);

% ---------------------------------------------------------------------- #
% Plot all data
% ---------------------------------------------------------------------- #

% Prepare data
age01  = data01.age;
u01    = data01.u0;
er01   = data01.err;
lbl01  = data01.lbl;

age02  = data02.age;
u02    = data02.u0;
er02   = data02.err;
lbl02  = data02.lbl;

% Prepare Figure
figure(1); clf;
hold on
grid on
box on

font_size  = 14;
size_color = 3;

% Axes
xmin = 0; xmax = 90;    % Age (Myr)
ymin = 0; ymax = 250;   % Rate (mm/yr)

% Colors
cgrey   = [0.7 0.7 0.7];
corange = [1.0 128/255 0.0]; % #FF8000
cred    = [204/255 0.0 0.0]; % #CC0000
cyellow = [1.0 215/255 0.0]; % #FFD700
cgreen  = [60/255 179/255 113/255]; % #3CB371
cblue   = [0.0 0.0 205/255]; % #0000CD

% Define delimiters
xstage0 = 90;
xstage1 = 72; % plume
xstage2 = 65; % max plume
xstage3 = 62; % drop, change in force balance
xstage4 = 60; % double subd
xstage5 = 50; % arc-collision/end of double subd
xstage6 = 45; % cont-collision
xstage7 =  0; % present-day

% Plot data delimiters (del01)
h01 = fill([xstage2 xstage1 xstage1 xstage2],[ymin ymin ymax ymax],corange,'FaceAlpha',0.4,'LineStyle','none'); % plume arrival
h02 = fill([xstage3 xstage2 xstage2 xstage3],[ymin ymin ymax ymax],cred,'FaceAlpha',0.4,'LineStyle','none'); % max plume
h03 = fill([xstage4 xstage3 xstage3 xstage4],[ymin ymin ymax ymax],cyellow,'FaceAlpha',0.4,'LineStyle','none'); % plume drop
h04 = fill([xstage5 xstage4 xstage4 xstage5],[ymin ymin ymax ymax],cgreen,'FaceAlpha',0.4,'LineStyle','none'); % double subd
h05 = fill([xstage6 xstage5 xstage5 xstage6],[ymin ymin ymax ymax],cblue,'FaceAlpha',0.4,'LineStyle','none'); % arc-cont collision

% Plot error bars
if (er01.agemin)
    h10 = patch([er01.agemin; fliplr(er01.agemax')'],[er01.umin; fliplr(er01.umax')'],'g');
end

% Plot data
h00 = plot(age01,u01,'k','LineWidth',2);
h01 = plot(age02,u02,'r:','LineWidth',2);

% Axes and labels
set(gca,'xdir','reverse');
axis([xmin xmax ymin ymax]);

% Labels
xlabel('Age (Myr)','FontSize',font_size);
ylabel('Rate (mm/yr)','FontSize',font_size);
set(gca,'FontSize',font_size);

h = legend([h00 h01],{lbl01,lbl02},'Location','NorthEast','FontSize',font_size-3,'Interpreter', 'none');
title(ttl, 'FontSize',font_size-2,'Interpreter', 'none');

% Create figure
set(gcf, 'Position', [0, 0, 500, 400]);
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 5.00 4.00])

% Save name
print(figure(1),[dir_out name_out],'-dpng');
