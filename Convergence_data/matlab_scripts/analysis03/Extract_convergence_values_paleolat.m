function [DataX] = Extract_convergence_values_paleolat(name,lbl,dir_in)
% Parameters:
%   name     - name of file
%   lbl      - plot label
%   dir_in   - directory of data

% Files needed (time Ma, paleolatitude):
%       <name>_eur.csv
%       <name>_ind.csv
%       <name>_eur_max.csv
%       <name>_eur_min.csv
%       <name>_ind_max.csv
%       <name>_ind_min.csv

% ---------------------------------------------------------------------- #
% Load main .csv data
% ---------------------------------------------------------------------- #
fname = [dir_in name(1:end-4) '_eur.csv'];
m_eur = importdata(fname);

fname = [dir_in name(1:end-4) '_ind.csv'];
m_ind = importdata(fname);

fname = [dir_in name(1:end-4) '_eur_max.csv'];
m_eur_max = importdata(fname);

fname = [dir_in name(1:end-4) '_eur_min.csv'];
m_eur_min = importdata(fname);

fname = [dir_in name(1:end-4) '_ind_max.csv'];
m_ind_max = importdata(fname);

fname = [dir_in name(1:end-4) '_ind_min.csv'];
m_ind_min = importdata(fname);

% Save data
eur.age = m_eur(:,1);
eur.pal = m_eur(:,2);

eur.agemax = m_eur_max(:,1);
eur.palmax = m_eur_max(:,2);

eur.agemin = m_eur_min(:,1);
eur.palmin = m_eur_min(:,2);

ind.age = m_ind(:,1);
ind.pal = m_ind(:,2);

ind.agemax = m_ind_max(:,1);
ind.palmax = m_ind_max(:,2);

ind.agemin = m_ind_min(:,1);
ind.palmin = m_ind_min(:,2);

% ---------------------------------------------------------------------- #
% Interpolate data on same grid 
% - in order to make operations (transform to convergence)
% ---------------------------------------------------------------------- #
% Find min time interval
t0 = max([min(eur.age) min(eur.agemax) min(eur.agemin) min(ind.age) min(ind.agemax) min(ind.agemin)]);
t1 = min([max(eur.age) max(eur.agemax) max(eur.agemin) max(ind.age) max(ind.agemax) max(ind.agemin)]);

n    = 100;
age0 = linspace(t0,t1,n);

% Interpolate values - paleolatitude
eur0    = interp1(eur.age   ,eur.pal,age0   );
ind0    = interp1(ind.age   ,ind.pal,age0   );
eurmax0 = interp1(eur.agemax,eur.palmax,age0);
eurmin0 = interp1(eur.agemin,eur.palmin,age0);
indmax0 = interp1(ind.agemax,ind.palmax,age0);
indmin0 = interp1(ind.agemin,ind.palmin,age0);

% ---------------------------------------------------------------------- #
% Calculate convergence
% ---------------------------------------------------------------------- #
% Paleolatitude difference (Transform paleolatitude to position)
lat2km = 111; % km

conv0    = (eur0-ind0).*lat2km;
convmax0 = (eurmax0-indmax0).*lat2km;
convmin0 = (eurmin0-indmin0).*lat2km;

% Calculate convergence rate [mm/yr]
age = (age0(2:end)+age0(1:end-1))./2;
u0  = (conv0(1:end-1)-conv0(2:end))./(age0(1:end-1)-age0(2:end));

err0.agemax = age';
err0.agemin = age';

u1   = (convmax0(1:end-1)-convmax0(2:end))./(age0(1:end-1)-age0(2:end));
u2   = (convmin0(1:end-1)-convmin0(2:end))./(age0(1:end-1)-age0(2:end));

for i=1:length(u1)
    umax(i)   = max([u1(i) u2(i)]);
    umin(i)   = min([u1(i) u2(i)]);
end

err0.umax = umax';
err0.umin = umin';

name_max = [name(1:end-4) '_max.csv'];
name_min = [name(1:end-4) '_min.csv'];

% Do not plot errors
err0.agemax = [];
err0.agemin = [];
err0.umax = [];
err0.umin = [];

name_max = '';
name_min = '';

% ---------------------------------------------------------------------- #
% Save structure
% ---------------------------------------------------------------------- #
DataX.name       = name;
DataX.name_max   = name_max;
DataX.name_min   = name_min;
DataX.lbl        = lbl;
DataX.age        = age;
DataX.u0         = u0;
DataX.err.agemax = err0.agemax;
DataX.err.agemin = err0.agemin;
DataX.err.umax   = err0.umax;
DataX.err.umin   = err0.umin;

end