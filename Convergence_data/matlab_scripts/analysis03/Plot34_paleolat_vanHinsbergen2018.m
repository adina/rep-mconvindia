% ---------------------------------------------------------------------- #
% Plot34_paleolat_vanHinsbergen2018.m
%
% Intended for "ConvIndia" convergence data from published studies
% Author: Adina Pusok, Mar 3, 2019
% ---------------------------------------------------------------------- #
clear
% ---------------------------------------------------------------------- #
% Directories
% ---------------------------------------------------------------------- #
% Input
dir_in = '../../digitized_plots/';

% Output
dir_out = '../../figures/analysis03/';

% Plot filename
name_out = 'Plot34_paleolat_vanHinsbergen2018.png';

% ---------------------------------------------------------------------- #
% List of .csv data structure
% ---------------------------------------------------------------------- #
name = 'n072_vanHinsbergen_2018_fig10.csv';

%name072 = 'n072_vanHinsbergen_2018_fig10.csv';
% ---------------------------------------------------------------------- #
% Error bars files
% ---------------------------------------------------------------------- #
%name_max = 'n003_cande_etal_2010_fig13_cir_max.csv';
%name_min = 'n003_cande_etal_2010_fig13_cir_min.csv';

% ---------------------------------------------------------------------- #
% Labels
% ---------------------------------------------------------------------- #
lbl = 'Paleolatitude - Ind-Eur';
ttl = 'van Hinsbergen et al. (2018)';

% ---------------------------------------------------------------------- #
% Load .csv data structure
% ---------------------------------------------------------------------- #
data = Extract_convergence_values_paleolat(name,lbl,dir_in);

%data054 = Extract_convergence_values_paleolat(name054,lbl054,dir_in);
% ---------------------------------------------------------------------- #
% Plot all data
% ---------------------------------------------------------------------- #

% Prepare data
age  = data.age;
u    = data.u0;
er   = data.err;
lbl  = data.lbl;

% Prepare Figure
figure(1); clf;
hold on
grid on
box on

font_size  = 14;
size_color = 3;

% Axes
xmin = 0; xmax = 90;    % Age (Myr)
ymin = 0; ymax = 250;   % Rate (mm/yr)

% Colors
cgrey   = [0.7 0.7 0.7];
corange = [1.0 128/255 0.0]; % #FF8000
cred    = [204/255 0.0 0.0]; % #CC0000
cyellow = [1.0 215/255 0.0]; % #FFD700
cgreen  = [60/255 179/255 113/255]; % #3CB371
cblue   = [0.0 0.0 205/255]; % #0000CD

% Define delimiters
xstage0 = 90;
xstage1 = 72; % plume
xstage2 = 65; % max plume
xstage3 = 62; % drop, change in force balance
xstage4 = 60; % double subd
xstage5 = 50; % arc-collision/end of double subd
xstage6 = 45; % cont-collision
xstage7 =  0; % present-day

% Plot data delimiters (del01)
h01 = fill([xstage2 xstage1 xstage1 xstage2],[ymin ymin ymax ymax],corange,'FaceAlpha',0.4,'LineStyle','none'); % plume arrival
h02 = fill([xstage3 xstage2 xstage2 xstage3],[ymin ymin ymax ymax],cred,'FaceAlpha',0.4,'LineStyle','none'); % max plume
h03 = fill([xstage4 xstage3 xstage3 xstage4],[ymin ymin ymax ymax],cyellow,'FaceAlpha',0.4,'LineStyle','none'); % plume drop
h04 = fill([xstage5 xstage4 xstage4 xstage5],[ymin ymin ymax ymax],cgreen,'FaceAlpha',0.4,'LineStyle','none'); % double subd
h05 = fill([xstage6 xstage5 xstage5 xstage6],[ymin ymin ymax ymax],cblue,'FaceAlpha',0.4,'LineStyle','none'); % arc-cont collision

% Plot error bars
if (er.agemin)
    h10 = patch([er.agemin; fliplr(er.agemax')'],[er.umin; fliplr(er.umax')'],'g');
end

% Plot data
h00 = plot(age,u,'k','LineWidth',2);

% Axes and labels
set(gca,'xdir','reverse');
axis([xmin xmax ymin ymax]);

% Labels
xlabel('Age (Myr)','FontSize',font_size);
ylabel('Rate (mm/yr)','FontSize',font_size);
set(gca,'FontSize',font_size);

h = legend(h00,{lbl},'Location','NorthEast','FontSize',font_size-3,'Interpreter', 'none');
title(ttl, 'FontSize',font_size-2,'Interpreter', 'none');

% Create figure
set(gcf, 'Position', [0, 0, 500, 400]);
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 5.00 4.00])

% Save name
print(figure(1),[dir_out name_out],'-dpng');
