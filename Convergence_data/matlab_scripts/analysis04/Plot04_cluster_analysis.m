% ---------------------------------------------------------------------- #
% Plot04_cluster_analysis.m
%
% Intended for "ConvIndia" convergence data from published studies
% Author: Adina Pusok, Mar 6, 2019
% ---------------------------------------------------------------------- #
clear
% ---------------------------------------------------------------------- #
% Directories
% ---------------------------------------------------------------------- #
% Input
dir_in = '../../digitized_plots/';

% Output
dir_out = '../../figures/analysis04/';

% Add path
addpath('../analysis01/');

% ---------------------------------------------------------------------- #
% Input
% ---------------------------------------------------------------------- #
n = 91;

% SEIR data (Ind-Ant)
name001 = 'n007_cande_patriat_2015_fig13_cap_ant_GTS12.csv';
name002 = 'n006_cande_patriat_2015_fig12_cap_ant_max.csv';
name003 = 'n006_cande_patriat_2015_fig12_cap_ant_min.csv';
name004 = 'n009_cande_patriat_2015_fig13_cap_ant_GTS04.csv';
name005 = 'n008_cande_patriat_2015_fig13_cap_ant_CK95.csv';

name006 = 'n022_cande_stegman_2011_fig2_ind_ant.csv';
name007 = 'n022_cande_stegman_2011_fig2_ind_ant_max.csv';
name008 = 'n022_cande_stegman_2011_fig2_ind_ant_min.csv';
name009 = 'n001_cande_etal_2010_fig11_seir_ck95.csv';
name010 = 'n017_cande_patriat_2015_fig16_cap_ant_ship.csv';

name011 = 'n019_cande_patriat_2015_fig16_cap_ant_ship_corr.csv';
name012 = 'n026_cande_stegman_2011_fig2_ind_ant_ship.csv';
name013 = 'n030_cande_stegman_2011_fig2_ind_ant_ship_corr.csv';
name014 = 'n016_cande_patriat_2015_fig16_cap_ant_rot.csv';
name015 = 'n018_cande_patriat_2015_fig16_cap_ant_rot_corr.csv';

name016 = 'n025_cande_stegman_2011_fig2_ind_ant_syn.csv';
name017 = 'n029_cande_stegman_2011_fig2_ind_ant_syn_corr.csv';

% CIR data (Ind-Afr)
name018 = 'n014_cande_patriat_2015_fig15_cap_afr_GTS12.csv';
name019 = 'n005_cande_patriat_2015_fig12_cap_afr_max.csv';
name020 = 'n005_cande_patriat_2015_fig12_cap_afr_min.csv';

name021 = 'n010_cande_patriat_2015_fig14_cap_afr_GTS04.csv';
name022 = 'n023_cande_stegman_2011_fig2_ind_afr.csv';
name023 = 'n023_cande_stegman_2011_fig2_ind_afr_max.csv';
name024 = 'n023_cande_stegman_2011_fig2_ind_afr_min.csv';
name025 = 'n024_cande_stegman_2011_fig2_ind_afr_syn.csv';

name026 = 'n028_cande_stegman_2011_fig2_ind_afr_syn_corr.csv';
name027 = 'n027_cande_stegman_2011_fig2_ind_afr_ship.csv';
name028 = 'n031_cande_stegman_2011_fig2_ind_afr_ship_corr.csv';
name029 = 'n033_cande_stegman_2011_fig3_ind_afr_ship.csv';
name030 = 'n021_cande_patriat_2015_fig17_cap_afr_eagles_hoang13.csv';

name031 = 'n076_White_Lister_2012_fig3_patriat1988.csv';
name032 = 'n077_White_Lister_2012_fig3_molnar1988.csv';

% India-Abs
name033 = 'n015_cande_patriat_2015_fig15_cap_abs_GTS12.csv';
name034 = 'n037_eagles_wibisino_2013_fig5_ind_abs1.csv';
name035 = 'n038_eagles_wibisino_2013_fig5_ind_abs2.csv';

name036 = 'n052_vanHinsbergen_etal_2011_fig3_ind_abs_AW.csv';
name037 = 'n053_vanHinsbergen_etal_2011_fig3_ind_abs_AE.csv';
name038 = 'n052_vanHinsbergen_etal_2011_fig3_ind_abs_AW_max.csv';
name039 = 'n052_vanHinsbergen_etal_2011_fig3_ind_abs_AW_min.csv';
name040 = 'n053_vanHinsbergen_etal_2011_fig3_ind_abs_AE_max.csv';

name041 = 'n053_vanHinsbergen_etal_2011_fig3_ind_abs_AE_min.csv';

% Paleolatitude
name042 = 'n072_vanHinsbergen_2018_fig10.csv';
name043 = 'n054_Hu_etal_2016_fig7.csv';
name044 = 'n055_meng_etal_2012_fig7.csv';
name045 = 'n070_vanHinsbergen_2012_fig2.csv';

% Plate circuit
name046 = 'n066_searle_2015_fig21.csv';
name047 = 'n080_White_Lister_2012_fig8.csv';
name048 = 'n069_shellnutt_2014_fig3_white2012.csv';
name049 = 'n082_Zahirovic_etal_2012_fig2_muller2008.csv';
name050 = 'n048_vanHinsbergen_etal_2011_fig3_ind_asia_AW.csv';

name051 = 'n049_vanHinsbergen_etal_2011_fig3_ind_asia_AE.csv';
name052 = 'n048_vanHinsbergen_etal_2011_fig3_ind_asia_AW_max.csv';
name053 = 'n048_vanHinsbergen_etal_2011_fig3_ind_asia_AW_min.csv';
name054 = 'n049_vanHinsbergen_etal_2011_fig3_ind_asia_AE_max.csv';
name055 = 'n049_vanHinsbergen_etal_2011_fig3_ind_asia_AE_min.csv';

name056 = 'n050_vanHinsbergen_etal_2011_fig3_ind_asia_BW.csv';
name057 = 'n051_vanHinsbergen_etal_2011_fig3_ind_asia_BE.csv';
name058 = 'n050_vanHinsbergen_etal_2011_fig3_ind_asia_BW_max.csv';
name059 = 'n050_vanHinsbergen_etal_2011_fig3_ind_asia_BW_min.csv';
name060 = 'n051_vanHinsbergen_etal_2011_fig3_ind_asia_BE_max.csv';

name061 = 'n051_vanHinsbergen_etal_2011_fig3_ind_asia_BE_min.csv';
name062 = 'n060_nerlich_etal_2016_fig1_vanHinsbergen2011.csv';
name063 = 'n067_shellnutt_2014_fig3_vanHinsbergen2011.csv';
name064 = 'n084_Zahirovic_etal_2012_fig2_vanHinsbergen2011.csv';
name065 = 'n034_copley_etal_2010_figA1_ind_asia.csv';

name066 = 'n068_shellnutt_2014_fig3_copley2010.csv';
name067 = 'n079_White_Lister_2012_fig4_copley2010_recalc.csv';
name068 = 'n035_copley_etal_2010_figA1_molnar_stock09.csv';
name069 = 'n036_copley_etal_2010_figA1_molnar_stock09_up.csv';
name070 = 'n083_Zahirovic_etal_2012_fig2_molnar2009.csv';

name071 = 'n063_nerlich_etal_2016_fig1_lee1995.csv';
name072 = 'n081_Zahirovic_etal_2012_fig2_lee1995.csv';
name073 = 'n073_White_Lister_2012_fig2_dewey1989.csv';
name074 = 'n075_White_Lister_2012_fig2_patriat1984.csv';
name075 = 'n056_meng_etal_2012_fig7_conv.csv';

% Plate reconstruction models
name076 = 'n057_nerlich_etal_2016_fig1_gibbons2015.csv';
name077 = 'n085_Zahirovic_etal_2015_fig4_mhs.csv';
name078 = 'n086_Zahirovic_etal_2015_fig4_vmsr.csv';
name079 = 'n087_Zahirovic_etal_2015_fig4_gmhrf.csv';
name080 = 'n088_Zahirovic_etal_2015_fig4_fhs.csv';

name081 = 'n042_gibbons_etal_2015_fig7_ind_eurW.csv';
name082 = 'n042_gibbons_etal_2015_fig7_ind_eurE.csv';
name083 = 'n043_gibbons_etal_2015_fig7_ind_koh.csv';
name084 = 'n045_gibbons_etal_2015_fig7_ind_ant.csv';
name085 = 'n046_gibbons_etal_2015_fig7_ind_mad.csv';

name086 = 'n047_gibbons_etal_2015_fig7_aus_ind.csv';
name087 = 'n065_nerlich_etal_2016_fig1_avg.csv';
name088 = 'n059_nerlich_etal_2016_fig1_zahirovic2012.csv';
name089 = 'n058_nerlich_etal_2016_fig1_seton2012.csv';
name090 = 'n041_faccenna_etal_2013_fig2_ind_abs_model.csv';
name091 = 'n040_faccenna_etal_2013_fig2_clb07_cir.csv';

% ---------------------------------------------------------------------- #
% Extract data
% ---------------------------------------------------------------------- #
% SEIR data (Ind-Ant)
data001 = Extract_convergence_values(name001,'','','',0,0,0,dir_in);
data002 = Extract_convergence_values(name002,'','','',0,0,0,dir_in);
data003 = Extract_convergence_values(name003,'','','',0,0,0,dir_in);
data004 = Extract_convergence_values(name004,'','','',0,0,0,dir_in);
data005 = Extract_convergence_values(name005,'','','',0,0,0,dir_in);

data006 = Extract_convergence_values(name006,'','','',0,0,0,dir_in);
data007 = Extract_convergence_values(name007,'','','',0,0,0,dir_in);
data008 = Extract_convergence_values(name008,'','','',0,0,0,dir_in);
data009 = Extract_convergence_values(name009,'','','',0,0,0,dir_in);
data010 = Extract_convergence_values(name010,'','','',0,0,0,dir_in);

data011 = Extract_convergence_values(name011,'','','',0,0,0,dir_in);
data012 = Extract_convergence_values(name012,'','','',0,0,0,dir_in);
data013 = Extract_convergence_values(name013,'','','',0,0,0,dir_in);
data014 = Extract_convergence_values(name014,'','','',0,0,0,dir_in);
data015 = Extract_convergence_values(name015,'','','',0,0,0,dir_in);

data016 = Extract_convergence_values(name016,'','','',0,0,0,dir_in);
data017 = Extract_convergence_values(name017,'','','',0,0,0,dir_in);

% CIR data (Ind-Afr)
data018 = Extract_convergence_values(name018,'','','',0,0,0,dir_in);
data019 = Extract_convergence_values(name019,'','','',0,0,0,dir_in);
data020 = Extract_convergence_values(name020,'','','',0,0,0,dir_in);

data021 = Extract_convergence_values(name021,'','','',0,0,0,dir_in);
data022 = Extract_convergence_values(name022,'','','',0,0,0,dir_in);
data023 = Extract_convergence_values(name023,'','','',0,0,0,dir_in);
data024 = Extract_convergence_values(name024,'','','',0,0,0,dir_in);
data025 = Extract_convergence_values(name025,'','','',0,0,0,dir_in);

data026 = Extract_convergence_values(name026,'','','',0,0,0,dir_in);
data027 = Extract_convergence_values(name027,'','','',0,0,0,dir_in);
data028 = Extract_convergence_values(name028,'','','',0,0,0,dir_in);
data029 = Extract_convergence_values(name029,'','','',0,0,0,dir_in);
data030 = Extract_convergence_values(name030,'','','',0,0,0,dir_in);

data031 = Extract_convergence_values(name031,'','','',0,0,0,dir_in);
data032 = Extract_convergence_values(name032,'','','',0,0,0,dir_in);

% India-Abs
data033 = Extract_convergence_values(name033,'','','',0,0,0,dir_in);
data034 = Extract_convergence_values(name034,'','','',0,0,0,dir_in);
data035 = Extract_convergence_values(name035,'','','',0,0,0,dir_in);

data036 = Extract_convergence_values(name036,'','','',0,0,0,dir_in);
data037 = Extract_convergence_values(name037,'','','',0,0,0,dir_in);
data038 = Extract_convergence_values(name038,'','','',0,0,0,dir_in);
data039 = Extract_convergence_values(name039,'','','',0,0,0,dir_in);
data040 = Extract_convergence_values(name040,'','','',0,0,0,dir_in);

data041 = Extract_convergence_values(name041,'','','',0,0,0,dir_in);

% Paleolatitude
data042 = Extract_convergence_values_paleolat(name042,'',dir_in);
data043 = Extract_convergence_values_paleolat(name043,'',dir_in);
data044 = Extract_convergence_values_paleolat(name044,'',dir_in);
data045 = Extract_convergence_values_paleolat(name045,'',dir_in);

% Plate circuit
data046 = Extract_convergence_values(name046,'','','',0,0,0,dir_in);
data047 = Extract_convergence_values(name047,'','','',0,0,0,dir_in);
data048 = Extract_convergence_values(name048,'','','',0,0,0,dir_in);
data049 = Extract_convergence_values(name049,'','','',0,0,0,dir_in);
data050 = Extract_convergence_values(name050,'','','',0,0,0,dir_in);

data051 = Extract_convergence_values(name051,'','','',0,0,0,dir_in);
data052 = Extract_convergence_values(name052,'','','',0,0,0,dir_in);
data053 = Extract_convergence_values(name053,'','','',0,0,0,dir_in);
data054 = Extract_convergence_values(name054,'','','',0,0,0,dir_in);
data055 = Extract_convergence_values(name055,'','','',0,0,0,dir_in);

data056 = Extract_convergence_values(name056,'','','',0,0,0,dir_in);
data057 = Extract_convergence_values(name057,'','','',0,0,0,dir_in);
data058 = Extract_convergence_values(name058,'','','',0,0,0,dir_in);
data059 = Extract_convergence_values(name059,'','','',0,0,0,dir_in);
data060 = Extract_convergence_values(name060,'','','',0,0,0,dir_in);

data061 = Extract_convergence_values(name061,'','','',0,0,0,dir_in);
data062 = Extract_convergence_values(name062,'','','',1,0,0,dir_in);
data063 = Extract_convergence_values(name063,'','','',0,0,0,dir_in);
data064 = Extract_convergence_values(name064,'','','',0,0,0,dir_in);
data065 = Extract_convergence_values(name065,'','','',0,0,0,dir_in);

data066 = Extract_convergence_values(name066,'','','',0,0,0,dir_in);
data067 = Extract_convergence_values(name067,'','','',0,0,0,dir_in);
data068 = Extract_convergence_values(name068,'','','',0,0,0,dir_in);
data069 = Extract_convergence_values(name069,'','','',0,0,0,dir_in);
data070 = Extract_convergence_values(name070,'','','',0,0,0,dir_in);

data071 = Extract_convergence_values(name071,'','','',1,0,0,dir_in);
data072 = Extract_convergence_values(name072,'','','',0,0,0,dir_in);
data073 = Extract_convergence_values(name073,'','','',0,0,0,dir_in);
data074 = Extract_convergence_values(name074,'','','',0,0,0,dir_in);
data075 = Extract_convergence_values(name075,'','','',1,0,0,dir_in);

% Plate reconstruction models
data076 = Extract_convergence_values(name076,'','','',1,0,0,dir_in);
data077 = Extract_convergence_values(name077,'','','',1,0,0,dir_in);
data078 = Extract_convergence_values(name078,'','','',1,0,0,dir_in);
data079 = Extract_convergence_values(name079,'','','',1,0,0,dir_in);
data080 = Extract_convergence_values(name080,'','','',1,0,0,dir_in);

data081 = Extract_convergence_values(name081,'','','',1,0,0,dir_in);
data082 = Extract_convergence_values(name082,'','','',1,0,0,dir_in);
data083 = Extract_convergence_values(name083,'','','',1,0,0,dir_in);
data084 = Extract_convergence_values(name084,'','','',0,0,0,dir_in);
data085 = Extract_convergence_values(name085,'','','',0,0,0,dir_in);

data086 = Extract_convergence_values(name086,'','','',0,0,0,dir_in);
data087 = Extract_convergence_values(name087,'','','',1,0,0,dir_in);
data088 = Extract_convergence_values(name088,'','','',1,0,0,dir_in);
data089 = Extract_convergence_values(name089,'','','',1,0,0,dir_in);
data090 = Extract_convergence_values(name090,'','','',1,0,0,dir_in);
data091 = Extract_convergence_values(name091,'','','',1,1,0,dir_in);

% ---------------------------------------------------------------------- #
% Analyse data
% ---------------------------------------------------------------------- #
% Create arrays
x = 1:91;
st01.max = zeros(size(x)); % orange
st02.max = zeros(size(x)); % red
st03.max = zeros(size(x)); % yellow
st04.max = zeros(size(x)); % green
st05.max = zeros(size(x)); % blue
st06.max = zeros(size(x)); % grey

st01.min = zeros(size(x)); % orange
st02.min = zeros(size(x)); % red
st03.min = zeros(size(x)); % yellow
st04.min = zeros(size(x)); % green
st05.min = zeros(size(x)); % blue
st06.min = zeros(size(x)); % grey

st01.avg = zeros(size(x)); % orange
st02.avg = zeros(size(x)); % red
st03.avg = zeros(size(x)); % yellow
st04.avg = zeros(size(x)); % green
st05.avg = zeros(size(x)); % blue
st06.avg = zeros(size(x)); % grey

% Define delimiters
xstage0 = 90;
xstage1 = 72; % plume
xstage2 = 65; % max plume
xstage3 = 62; % drop, change in force balance
xstage4 = 60; % double subd
xstage5 = 50; % arc-collision/end of double subd
xstage6 = 45; % cont-collision
xstage7 =  0; % present-day

% Dummies
%xage = 0:0.1:80; % time

% Calculate data
for i=1:n
    
    % Create name
    if i<10
        name = ['data = data00' num2str(i) ';'];
    else
        name = ['data = data0' num2str(i) ';'];
    end
    
    % Extract data
    eval(name);
    
    % Flip data if t(1)>t(end)
    if data.age(end)<data.age(1)
        a = flipud(data.age);
        b = flipud(data.u0);
        data.age = a;
        data.u0  = b;
    end
    
    %disp([num2str(i),' T1 = ',num2str(data.age(1)),' T2 = ',num2str(data.age(end))]);
    
    % First interpolate data to a finer grid (xage, xu0)
    %     xu0  = zeros(size(xage));
    %
    %     [ind] = find(xage>=data.age(1) & xage<=data.age(end));
    %     xu0(ind) = interp1(data.age, data.u0, xage(ind));
    
    % Calculate values
    t0    = min(data.age);
    t1    = max(data.age);
    
    %%%% STAGE 1 %%%%
    [ind] = find(data.age<=xstage1 & data.age>=xstage2);
    
    if length(ind)>=3
        xu0  = data.u0(ind);
        xage = data.age(ind);
    elseif length(ind)==2
        ii1 = ind(1);
        ii2 = ind(2);
        
        if data.age(ii1)==t0
            ii0=ii1;
        else
            ii0=ii1-1;
        end
        
        if data.age(ii2)==t1
            ii3=ii2;
        else
            ii3=ii2+1;
        end
        
        xu0 = data.u0(ii0:ii3);
        xage = data.age(ii0:ii3);
    elseif length(ind)==1
        ii1 = ind(1);
        
        if data.age(ii1)==t0
            ii0=ii1;
        else
            ii0=ii1-1;
        end
        
        if data.age(ii1)==t1
            ii2=ii1;
        else
            ii2=ii1+1;
        end
        
        xu0 = data.u0(ii0:ii2);
        xage = data.age(ii0:ii2);
    else
        % outside the area
        if (t1<xstage2) || (t0>xstage1)
            xu0 = 0;
            xage = 0;
        else
            tstage = (xstage1+xstage2)/2;
            tage = data.age-tstage;
            age0 = min(abs(tage));
            [ii1] = find(tage==age0);
            
            if data.age(ii1)>xstage1
                ii0 = ii1-1;
                ii2 = ii1;
            elseif data.age(ii1)<xstage2
                ii0 = ii1;
                ii2 = ii1+1;
            end
            
            xu0 = data.u0(ii0:ii2);
            xage = data.age(ii0:ii2);
        end
    end
    
    %     clf; plot(data.age,data.u0,'k-'); hold on
    %     plot(xage, xu0,'r*');
    
    st01.max(i) = max(xu0);
    st01.min(i) = min(xu0);
    st01.avg(i) = mean(xu0);
    
    %%%% STAGE 2 %%%%
    [ind] = find(data.age<=xstage2 & data.age>=xstage3);
    
    if length(ind)>=3
        xu0  = data.u0(ind);
        xage = data.age(ind);
    elseif length(ind)==2
        ii1 = ind(1);
        ii2 = ind(2);
        
        if data.age(ii1)==t0
            ii0=ii1;
        else
            ii0=ii1-1;
        end
        
        if data.age(ii2)==t1
            ii3=ii2;
        else
            ii3=ii2+1;
        end
        
        xu0 = data.u0(ii0:ii3);
        xage = data.age(ii0:ii3);
    elseif length(ind)==1
        ii1 = ind(1);
        
        if data.age(ii1)==t0
            ii0=ii1;
        else
            ii0=ii1-1;
        end
        
        if data.age(ii1)==t1
            ii2=ii1;
        else
            ii2=ii1+1;
        end
        
        xu0 = data.u0(ii0:ii2);
        xage = data.age(ii0:ii2);
    else
        % outside the area
        if (t1<xstage3) || (t0>xstage2)
            xu0 = 0;
            xage = 0;
        else
            tstage = (xstage2+xstage3)/2;
            tage = data.age-tstage;
            age0 = min(abs(tage));
            [ii1] = find(tage==age0);
            
            if data.age(ii1)>xstage2
                ii0 = ii1-1;
                ii2 = ii1;
            elseif data.age(ii1)<xstage3
                ii0 = ii1;
                ii2 = ii1+1;
            end
            
            xu0 = data.u0(ii0:ii2);
            xage = data.age(ii0:ii2);
        end
    end
    
    %     plot(xage, xu0,'b*');
    
    st02.max(i) = max(xu0);
    st02.min(i) = min(xu0);
    st02.avg(i) = mean(xu0);
    
    %%%% STAGE 3 %%%%
    [ind] = find(data.age<=xstage3 & data.age>=xstage4);
    
    if length(ind)>=3
        xu0  = data.u0(ind);
        xage = data.age(ind);
    elseif length(ind)==2
        ii1 = ind(1);
        ii2 = ind(2);
        
        if data.age(ii1)==t0
            ii0=ii1;
        else
            ii0=ii1-1;
        end
        
        if data.age(ii2)==t1
            ii3=ii2;
        else
            ii3=ii2+1;
        end
        
        xu0 = data.u0(ii0:ii3);
        xage = data.age(ii0:ii3);
    elseif length(ind)==1
        ii1 = ind(1);
        
        if data.age(ii1)==t0
            ii0=ii1;
        else
            ii0=ii1-1;
        end
        
        if data.age(ii1)==t1
            ii2=ii1;
        else
            ii2=ii1+1;
        end
        
        xu0 = data.u0(ii0:ii2);
        xage = data.age(ii0:ii2);
    else
        % outside the area
        if (t1<xstage4) || (t0>xstage3)
            xu0 = 0;
            xage = 0;
        else
            tstage = (xstage3+xstage4)/2;
            tage = data.age-tstage;
            age0 = min(abs(tage));
            [ii1] = find(tage==age0);
            
            if data.age(ii1)>xstage3
                ii0 = ii1-1;
                ii2 = ii1;
            elseif data.age(ii1)<xstage4
                ii0 = ii1;
                ii2 = ii1+1;
            end
            
            xu0 = data.u0(ii0:ii2);
            xage = data.age(ii0:ii2);
        end
    end
    
    %     plot(xage, xu0,'g*');
    
    st03.max(i) = max(xu0);
    st03.min(i) = min(xu0);
    st03.avg(i) = mean(xu0);
    
    %%%% STAGE 4 %%%%
    [ind] = find(data.age<=xstage4 & data.age>=xstage5);
    
    if length(ind)>=3
        xu0  = data.u0(ind);
        xage = data.age(ind);
    elseif length(ind)==2
        ii1 = ind(1);
        ii2 = ind(2);
        
        if data.age(ii1)==t0
            ii0=ii1;
        else
            ii0=ii1-1;
        end
        
        if data.age(ii2)==t1
            ii3=ii2;
        else
            ii3=ii2+1;
        end
        
        xu0 = data.u0(ii0:ii3);
        xage = data.age(ii0:ii3);
    elseif length(ind)==1
        ii1 = ind(1);
        
        if data.age(ii1)==t0
            ii0=ii1;
        else
            ii0=ii1-1;
        end
        
        if data.age(ii1)==t1
            ii2=ii1;
        else
            ii2=ii1+1;
        end
        
        xu0 = data.u0(ii0:ii2);
        xage = data.age(ii0:ii2);
    else
        % outside the area
        if (t1<xstage5) || (t0>xstage4)
            xu0 = 0;
            xage = 0;
        else
            tstage = (xstage4+xstage5)/2;
            tage = data.age-tstage;
            age0 = min(abs(tage));
            [ii1] = find(tage==age0);
            
            if data.age(ii1)>xstage4
                ii0 = ii1-1;
                ii2 = ii1;
            elseif data.age(ii1)<xstage5
                ii0 = ii1;
                ii2 = ii1+1;
            end
            
            xu0 = data.u0(ii0:ii2);
            xage = data.age(ii0:ii2);
        end
    end
    
    %     plot(xage, xu0,'c*');
    
    st04.max(i) = max(xu0);
    st04.min(i) = min(xu0);
    st04.avg(i) = mean(xu0);
    
    %%%% STAGE 5 %%%%
    [ind] = find(data.age<=xstage5 & data.age>=xstage6);
    
    if length(ind)>=3
        xu0  = data.u0(ind);
        xage = data.age(ind);
    elseif length(ind)==2
        ii1 = ind(1);
        ii2 = ind(2);
        
        if data.age(ii1)==t0
            ii0=ii1;
        else
            ii0=ii1-1;
        end
        
        if data.age(ii2)==t1
            ii3=ii2;
        else
            ii3=ii2+1;
        end
        
        xu0 = data.u0(ii0:ii3);
        xage = data.age(ii0:ii3);
    elseif length(ind)==1
        ii1 = ind(1);
        
        if data.age(ii1)==t0
            ii0=ii1;
        else
            ii0=ii1-1;
        end
        
        if data.age(ii1)==t1
            ii2=ii1;
        else
            ii2=ii1+1;
        end
        
        xu0 = data.u0(ii0:ii2);
        xage = data.age(ii0:ii2);
    else
        % outside the area
        if (t1<xstage6) || (t0>xstage5)
            xu0 = 0;
            xage = 0;
        else
            tstage = (xstage5+xstage6)/2;
            tage = data.age-tstage;
            age0 = min(abs(tage));
            [ii1] = find(tage==age0);
            
            if data.age(ii1)>xstage5
                ii0 = ii1-1;
                ii2 = ii1;
            elseif data.age(ii1)<xstage6
                ii0 = ii1;
                ii2 = ii1+1;
            end
            
            xu0 = data.u0(ii0:ii2);
            xage = data.age(ii0:ii2);
        end
    end
    
    % plot(xage, xu0,'m*');
    
    st05.max(i) = max(xu0);
    st05.min(i) = min(xu0);
    st05.avg(i) = mean(xu0);
    
    %%%% STAGE 6 %%%%
    [ind] = find(data.age<=xstage6);
    
    if length(ind)>=3
        xu0  = data.u0(ind);
        xage = data.age(ind);
    elseif length(ind)==2
        ii1 = ind(1);
        ii2 = ind(2);
        
        if data.age(ii1)==t0
            ii0=ii1;
        else
            ii0=ii1-1;
        end
        
        if data.age(ii2)==t1
            ii3=ii2;
        else
            ii3=ii2+1;
        end
        
        xu0 = data.u0(ii0:ii3);
        xage = data.age(ii0:ii3);
    elseif length(ind)==1
        ii1 = ind(1);
        
        if data.age(ii1)==t0
            ii0=ii1;
        else
            ii0=ii1-1;
        end
        
        if data.age(ii1)==t1
            ii2=ii1;
        else
            ii2=ii1+1;
        end
        
        xu0 = data.u0(ii0:ii2);
        xage = data.age(ii0:ii2);
    else
        %         % outside the area
        %         if (t0>xstage6)
        xu0 = 0;
        xage = 0;
        %         else
        %             tstage = (xstage5+xstage6)/2;
        %             tage = data.age-tstage;
        %             age0 = min(abs(tage));
        %             [ii1] = find(tage==age0);
        %
        %             if data.age(ii1)>xstage5
        %                 ii0 = ii1-1;
        %                 ii2 = ii1;
        %             elseif data.age(ii1)<xstage6
        %                 ii0 = ii1;
        %                 ii2 = ii1+1;
        %             end
        %
        %             xu0 = data.u0(ii0:ii2);
        %             xage = data.age(ii0:ii2);
        %         end
    end
    
    % plot(xage, xu0,'m*');
    
    st06.max(i) = max(xu0);
    st06.min(i) = min(xu0);
    st06.avg(i) = mean(xu0);
    
    clearvars data xu0 xage ind a b
    
end

% ---------------------------------------------------------------------- #
% Plot all data
% ---------------------------------------------------------------------- #
% Prepare Figure
figure(1); clf;
hold on
grid on
box on

font_size  = 14;
size_color = 3;

% Axes
xmin = 0; xmax = 92;    % Samples
ymin = 0; ymax = 250;   % Rate (mm/yr)

% Colors
cgrey   = [0.7 0.7 0.7];
corange = [1.0 128/255 0.0]; % #FF8000
cred    = [204/255 0.0 0.0]; % #CC0000
cyellow = [1.0 215/255 0.0]; % #FFD700
cgreen  = [60/255 179/255 113/255]; % #3CB371
cblue   = [0.0 0.0 205/255]; % #0000CD

% Plot method divider
x0 = 17.5;
x1 = 32.5;
x2 = 41.5;
x3 = 45.5;
x4 = 75.5;
y  = ymin:ymax;
plot(x0*ones(size(y)),y,'-','Color',cgrey,'LineWidth',2);
plot(x1*ones(size(y)),y,'-','Color',cgrey,'LineWidth',2);
plot(x2*ones(size(y)),y,'-','Color',cgrey,'LineWidth',2);
plot(x3*ones(size(y)),y,'-','Color',cgrey,'LineWidth',2);
plot(x4*ones(size(y)),y,'-','Color',cgrey,'LineWidth',2);

% Choose value
% st     = st01.avg; %st01.max, st01.min, st01.avg
% ccolor = corange;

% st     = st02.avg; %st02.max, st02.min, st02.avg
% ccolor = cred;

% st     = st03.avg; %st03.max, st03.min, st03.avg
% ccolor = cyellow;

% st     = st04.avg; %st04.max, st04.min, st04.avg
% ccolor = cgreen;

% st     = st05.avg; %st05.max, st05.min, st05.avg
% ccolor = cblue;

st     = st06.avg; %st06.max, st06.min, st06.avg
ccolor = cgrey;

% Plot values
[ind] = find(st>0); plot(x(ind),st(ind),'o','MarkerSize',10,'MarkerFaceColor',ccolor);
avgtot = mean(st(ind));

% Averages
[ind] = find(st>0 & x < x0); avg00 = mean(st(ind));
plot(x(1:17),avg00*ones(size(x(1:17))),'-','Color','k','LineWidth',5);

[ind] = find(st>0 & x > x0 & x < x1); avg01 = mean(st(ind));
plot(x(18:32),avg01*ones(size(x(18:32))),'-','Color','k','LineWidth',5);

[ind] = find(st>0 & x > x1 & x < x2); avg02 = mean(st(ind));
plot(x(33:41),avg02*ones(size(x(33:41))),'-','Color','k','LineWidth',5);

[ind] = find(st>0 & x > x2 & x < x3); avg03 = mean(st(ind));
plot(x(42:45),avg03*ones(size(x(42:45))),'-','Color','k','LineWidth',5);

[ind] = find(st>0 & x > x3 & x < x4); avg04 = mean(st(ind));
plot(x(46:75),avg04*ones(size(x(46:75))),'-','Color','k','LineWidth',5);

[ind] = find(st>0 & x > x4); avg05 = mean(st(ind));
plot(x(76:end),avg05*ones(size(x(76:end))),'-','Color','k','LineWidth',5);

disp(['Averages are: ',num2str(round(avg00,2)),' ',num2str(round(avg01,2)),' ',num2str(round(avg02,2)),' ',num2str(round(avg03,2)),' ',num2str(round(avg04,2)),' ',num2str(round(avg05,2)),' ',num2str(round(avgtot,2))]);

%[ind] = find(st02.max>0); plot(x(ind),st02.max(ind),'o','MarkerSize',10,'MarkerFaceColor',cred);
%[ind] = find(st03.max>0); plot(x(ind),st03.max(ind),'o','MarkerSize',10,'MarkerFaceColor',cyellow);
%[ind] = find(st04.max>0); plot(x(ind),st04.max(ind),'o','MarkerSize',10,'MarkerFaceColor',cgreen);
%[ind] = find(st05.max>0); plot(x(ind),st05.max(ind),'o','MarkerSize',10,'MarkerFaceColor',cblue);

%[ind] = find(st01.min>0); plot(x(ind),st01.min(ind),'o','MarkerSize',10,'MarkerFaceColor',corange);
%[ind] = find(st02.min>0); plot(x(ind),st02.min(ind),'o','MarkerSize',10,'MarkerFaceColor',cred);
%[ind] = find(st03.min>0); plot(x(ind),st03.min(ind),'o','MarkerSize',10,'MarkerFaceColor',cyellow);
%[ind] = find(st04.min>0); plot(x(ind),st04.min(ind),'o','MarkerSize',10,'MarkerFaceColor',cgreen);
%[ind] = find(st05.min>0); plot(x(ind),st05.min(ind),'o','MarkerSize',10,'MarkerFaceColor',cblue);

% Axes and labels
%set(gca,'xdir','reverse');
axis([xmin xmax ymin ymax]);

% Labels
xlabel('Samples','FontSize',font_size);
ylabel('Rate (mm/yr)','FontSize',font_size);
set(gca,'FontSize',font_size);

%h = legend(h00,{lbl},'Location','NorthEast','FontSize',font_size-3,'Interpreter', 'none');
%title(ttl, 'FontSize',font_size-2,'Interpreter', 'none');

% Create figure
set(gcf, 'Position', [0, 0, 800, 400]);
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8.00 4.00])

% Save name
%print(figure(1),[dir_out name_out],'-dpng');