% ---------------------------------------------------------------------- #
% Plot average convergence history
% ---------------------------------------------------------------------- #
clear
% ---------------------------------------------------------------------- #
% Directories
% ---------------------------------------------------------------------- #
% Input
dir_in = '../../digitized_plots/';

% Output
dir_out = '../../figures/analysis04/';

% ---------------------------------------------------------------------- #
% Data for each stage - from Plot04
% ---------------------------------------------------------------------- #

st01min = [92.71 62.81 80.75 98.61 103.3 82.88 88.58];
st01max = [178.98 175.92 160.34 121.67 148.42 131.93 155.7];
st01avg = [132.39 118.39 119.2 110.6 125.57 107.76 121.18];

st02min = [138.45 133.25 99.01 114.48 121.26 126.03 124.87];
st02max = [183.22 184.05 160.39 117.13 153.66 149.22 162.67];
st02avg = [161.22 158.49 130.57 115.36 142.14 137.12 145.26];

st03min = [127.32 111.68 131.31 114.48 137.7 125.07 127.93];
st03max = [136.09 129.41 146.63 121.32 149.48 149.45 142.57];
st03avg = [131.48 120.75 138.6 117.64 144.02 138.72 135.61];

st04min = [119.26 93.74 93.79 122.59 104.47 112.46 106.78];
st04max = [135.54 135.95 161.37 133.4 155.43 161.76 150.45];
st04avg = [128.85 114.8 137.28 131.07 136 139.41 132.56];

st05min = [68.56 43.58 80.49 101.15 86.02 86.06 78.67];
st05max = [121.95 110.44 95.15 115.13 113.62 121.52 114.23];
st05avg = [95.19 79.84 88.81 106.05 100.84 102.77 96.98];

st06min = [49.9 24.25 34.85 37.76 43.2 41.35 40.05];
st06max = [69.76 45.25 88.54 101.74 95.96 90.08 84.57];
st06avg = [57.45 33.77 54.04 56.97 60.17 53.85 54.31];

% ---------------------------------------------------------------------- #
% Prepare data
% ---------------------------------------------------------------------- #
% Define delimiters
xstage0 = 90;
xstage1 = 72; % plume
xstage2 = 65; % max plume
xstage3 = 62; % drop, change in force balance
xstage4 = 60; % double subd
xstage5 = 50; % arc-collision/end of double subd
xstage6 = 45; % cont-collision
xstage7 =  0; % present-day

% ---------------------------------------------------------------------- #
% Plot average convergence history
% ---------------------------------------------------------------------- #
% Prepare Figure
figure(1); clf;
hold on
grid on
box on

font_size  = 14;
size_color = 3;

% Axes
xmin = 0; xmax = 90;    % Age (myr)
ymin = 0; ymax = 250;   % Rate (mm/yr)

% Colors
cgrey   = [0.7 0.7 0.7];
corange = [1.0 128/255 0.0]; % #FF8000
cred    = [204/255 0.0 0.0]; % #CC0000
cyellow = [1.0 215/255 0.0]; % #FFD700
cgreen  = [60/255 179/255 113/255]; % #3CB371
cblue   = [0.0 0.0 205/255]; % #0000CD

% Individual plots
i   = 7;
% lbl = 'A. SEIR spreading (Ind-Ant)';
% lbl = 'B. CIR spreading (Ind-Afr)';
% lbl = 'C. Indian Ocean spreading (Ind-Abs)';
% lbl = 'D. Paleolatitude (Ind-Eur)';
% lbl = 'E. Plate circuit (Ind-Eur)';
% lbl = 'F. Plate reconstruction models';
lbl = 'Average all';

% Plot data
h01 = fill([xstage2 xstage1 xstage1 xstage2],[st01min(i) st01min(i) st01max(i) st01max(i)],corange,'FaceAlpha',0.4,'LineStyle','none');
h02 = fill([xstage3 xstage2 xstage2 xstage3],[st02min(i) st02min(i) st02max(i) st02max(i)],cred,'FaceAlpha',0.4,'LineStyle','none');
h03 = fill([xstage4 xstage3 xstage3 xstage4],[st03min(i) st03min(i) st03max(i) st03max(i)],cyellow,'FaceAlpha',0.4,'LineStyle','none');
h04 = fill([xstage5 xstage4 xstage4 xstage5],[st04min(i) st04min(i) st04max(i) st04max(i)],cgreen,'FaceAlpha',0.4,'LineStyle','none');
h05 = fill([xstage6 xstage5 xstage5 xstage6],[st05min(i) st05min(i) st05max(i) st05max(i)],cblue,'FaceAlpha',0.4,'LineStyle','none');
h06 = fill([xstage7 xstage6 xstage6 xstage7],[st06min(i) st06min(i) st06max(i) st06max(i)],cgrey,'FaceAlpha',0.4,'LineStyle','none');

h11 = plot([xstage2 xstage1],[st01avg(i) st01avg(i)],'-','Color',corange,'LineWidth',3);
h12 = plot([xstage3 xstage2],[st02avg(i) st02avg(i)],'-','Color',cred,'LineWidth',3);
h13 = plot([xstage4 xstage3],[st03avg(i) st03avg(i)],'-','Color',cyellow,'LineWidth',3);
h14 = plot([xstage5 xstage4],[st04avg(i) st04avg(i)],'-','Color',cgreen,'LineWidth',3);
h15 = plot([xstage6 xstage5],[st05avg(i) st05avg(i)],'-','Color',cblue,'LineWidth',3);
h16 = plot([xstage7 xstage6],[st06avg(i) st06avg(i)],'-','Color',cgrey,'LineWidth',3);

% Axes and labels
set(gca,'xdir','reverse');
axis([xmin xmax ymin ymax]);

% Labels
xlabel('Age (Myr)','FontSize',font_size);
ylabel('Rate (mm/yr)','FontSize',font_size);
set(gca,'FontSize',font_size);

%h = text(60,225,lbl,'FontSize',font_size);
%h = legend(h00,{lbl},'Location','NorthEast','FontSize',font_size-3,'Interpreter', 'none');
title(lbl, 'FontSize',font_size-2,'Interpreter', 'none');

% Create figure
set(gcf, 'Position', [0, 0, 500, 400]);
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 5.00 4.00])

% Save name
%print(figure(1),[dir_out 'averages'],'-dpng');

% ---------------------------------------------------------------------- #
% Plot best fit convergence history
% ---------------------------------------------------------------------- #
% Prepare Figure
figure(2); clf;
hold on
grid on
box on

font_size  = 14;
size_color = 3;

% Axes
xmin = 0; xmax = 90;    % Age (myr)
ymin = 0; ymax = 250;   % Rate (mm/yr)

% Colors
cgrey   = [0.7 0.7 0.7];
corange = [1.0 128/255 0.0]; % #FF8000
cred    = [204/255 0.0 0.0]; % #CC0000
cyellow = [1.0 215/255 0.0]; % #FFD700
cgreen  = [60/255 179/255 113/255]; % #3CB371
cblue   = [0.0 0.0 205/255]; % #0000CD

lbl = 'Best average';

% Individual plots
m01min = min(st01avg(1:6));
m01max = max(st01avg(1:6));
m01avg = st01avg(7);

m02min = min(st02max(1:6));
m02max = max(st02max(1:6));
m02avg = st02max(7);

m03min = min(st03min(1:6));
m03max = max(st03min(1:6));
m03avg = st03min(7);

m04min = min(st04max(1:6));
m04max = max(st04max(1:6));
m04avg = st04max(7);

m05min = min(st05avg(1:6));
m05max = max(st05avg(1:6));
m05avg = st05avg(7);

m06min = min(st06avg(1:6));
m06max = max(st06avg(1:6));
m06avg = st06avg(7);

% Plot data
h01 = fill([xstage2 xstage1 xstage1 xstage2],[m01min m01min m01max m01max],corange,'FaceAlpha',0.4,'LineStyle','none');
h02 = fill([xstage3 xstage2 xstage2 xstage3],[m02min m02min m02max m02max],cred,'FaceAlpha',0.4,'LineStyle','none');
h03 = fill([xstage4 xstage3 xstage3 xstage4],[m03min m03min m03max m03max],cyellow,'FaceAlpha',0.4,'LineStyle','none');
h04 = fill([xstage5 xstage4 xstage4 xstage5],[m04min m04min m04max m04max],cgreen,'FaceAlpha',0.4,'LineStyle','none');
h05 = fill([xstage6 xstage5 xstage5 xstage6],[m05min m05min m05max m05max],cblue,'FaceAlpha',0.4,'LineStyle','none');
h06 = fill([xstage7 xstage6 xstage6 xstage7],[m06min m06min m06max m06max],cgrey,'FaceAlpha',0.4,'LineStyle','none');

h11 = plot([xstage2 xstage1],[m01avg m01avg],'-','Color',corange,'LineWidth',3);
h12 = plot([xstage3 xstage2],[m02avg m02avg],'-','Color',cred,'LineWidth',3);
h13 = plot([xstage4 xstage3],[m03avg m03avg],'-','Color',cyellow,'LineWidth',3);
h14 = plot([xstage5 xstage4],[m04avg m04avg],'-','Color',cgreen,'LineWidth',3);
h15 = plot([xstage6 xstage5],[m05avg m05avg],'-','Color',cblue,'LineWidth',3);
h16 = plot([xstage7 xstage6],[m06avg m06avg],'-','Color',cgrey,'LineWidth',3);

% Axes and labels
set(gca,'xdir','reverse');
axis([xmin xmax ymin ymax]);

% Labels
xlabel('Age (Myr)','FontSize',font_size);
ylabel('Rate (mm/yr)','FontSize',font_size);
set(gca,'FontSize',font_size);

%h = text(60,225,lbl,'FontSize',font_size);
%h = legend(h00,{lbl},'Location','NorthEast','FontSize',font_size-3,'Interpreter', 'none');
title(lbl, 'FontSize',font_size-2,'Interpreter', 'none');

% Create figure
set(gcf, 'Position', [0, 0, 500, 400]);
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 5.00 4.00])