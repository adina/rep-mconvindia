% calculate grid resolution based on LaMEM input parameters

%% ModelB - ConvIndia
L     = 6000;
H     = 2048;

xs    = 0;
zs    = -1988;

nel_x = -6; %1024 
nel_z = -2; %512

seg_x = [1000 2000 3000 4000 5000 80 216 216 216 216 80 1.0 0.5 2.0 0.5 2.0 1.0];
seg_z = [-900 64 192 0.5 1.0];

% Resolution (grid) in km: [xmin, xmax] = 3.0864,12.5 [zmin, zmax] = 5,22.6667

%% Total number of cells
nx = 0;
for i=1:abs(nel_x)
    nx = nx + seg_x(abs(nel_x)-1+i);
end

nz = 0;
for i=1:abs(nel_z)
    nz = nz + seg_z(abs(nel_z)-1+i);
end

% Create grid
x = zeros(nx+1,1);
z = zeros(nz+1,1);

ni0 = 0;
for i=1:abs(nel_x)
    
    % no cells in segments
    ni  = seg_x(abs(nel_x)-1+i); 
    
    % delimiters
    if i==1
        xsi = xs;
    else 
        xsi = seg_x(i-1);
    end
    
    if i==abs(nel_x)
        xei = xs+L;
    else
        xei = seg_x(i);
    end
    
    % bias
    bi = seg_x(abs(nel_x)*2-1+i);
    
    % average cells size
    dxi = (xei-xsi)/ni;
    
    % uniform case
    if bi == 1
        x(ni0+1:ni0+ni+1) = xsi:dxi:xei;
    else
        % non uniform case
        x(ni0+1   ) = xsi;
        x(ni0+ni+1) = xei;
        
        bsi = 2*dxi/(1+bi);
        bei = bi*bsi;
        
        dx = (bei-bsi)/(ni-1); %ni-1?
        
        sum = 0;
        for ii=1:(ni-1)
            x(ni0+1+ii) = xsi + ii*bsi+sum*dx;
            sum = sum+ii;
        end
        
    end
    
    % update no of cells
    ni0 = ni0+ni;
end

ni0 = 0;
for i=1:abs(nel_z)
    
    % no cells in segments
    ni  = seg_z(abs(nel_z)-1+i); 
    
    % delimiters
    if i==1
        zsi = zs;
    else 
        zsi = seg_z(i-1);
    end
    
    if i==abs(nel_z)
        zei = zs+H;
    else
        zei = seg_z(i);
    end
    
    % bias
    bi = seg_z(abs(nel_z)*2-1+i);
    
    % average cells size
    dzi = (zei-zsi)/ni;
    
    % uniform case
    if bi == 1
        z(ni0+1:ni0+ni+1) = zsi:dzi:zei;
    else
        % non uniform case
        z(ni0+1   ) = zsi;
        z(ni0+ni+1) = zei;
        
        bsi = 2*dzi/(1+bi);
        bei = bi*bsi;
        
        dz = (bei-bsi)/(ni-1); %ni-1?
        
        sum = 0;
        for ii=1:(ni-1)
            z(ni0+1+ii) = zsi + ii*bsi+sum*dz;
            sum = sum+ii;
        end
        
    end
    
    % update no of cells
    ni0 = ni0+ni;
end

% M1  = importdata('Lower_mantle_res512/grid_info/grid_x.csv');
% M2  = importdata('Lower_mantle_res512/grid_info/grid_z.csv');
% x1 = M1.data(:,28);
% z1 = M2.data(:,30);

% Calculate resolution
xmin = min(abs(x(2:end)-x(1:end-1)));
xmax = max(abs(x(2:end)-x(1:end-1)));

zmin = min(abs(z(2:end)-z(1:end-1)));
zmax = max(abs(z(2:end)-z(1:end-1)));

disp(['Resolution (grid) in km: [xmin, xmax] = ',num2str(xmin),',',num2str(xmax),' [zmin, zmax] = ',num2str(zmin),',',num2str(zmax)]);
