% --------------------------------------------------%
%%%%% 2D CONVERGENCE INDIA ASIA 2018 - DOUBLE SUBD %%%%%
%%%%% RESOLUTION [1024X256X2]
% --------------------------------------------------%

% This script creates LaMEM input files (parallel and/or sequential) for markers
% Files contain: marker coordinates, phase and temperature distributions
% WARNING: The model setup should be dimensional! Non-dimensionalization is done internally in LaMEM!
% WARNING: units should be consistent with the input file
%          old_input = [m, deg C]
%          new_input (units=si ) = [m,  deg K]
%          new_input (units=geo) = [km, deg C]

clear
%addpath ../../../matlab

%==========================================================================
% OUTPUT OPTIONS
%==========================================================================
% See model setup in Paraview 1-YES; 0-NO
Paraview_output        = 0;

% Output a single file containing particles information for LaMEM (msetup = redundant)
LaMEM_Redundant_output = 0;

% Output parallel files for LaMEM, using a processor distribution file (msetup = parallel)
% WARNING: Need a valid 'Parallel_partition' file!
LaMEM_Parallel_output  = 1;

% Mesh from file 1-YES (load uniform or variable mesh from file); 0-NO (create new uniform mesh)
% WARNING: Need a valid 'Parallel_partition' file!
LoadMesh               = 1;

% Parallel partition file
Parallel_partition     = 'ProcessorPartitioning_24cpu_8.1.3_model.bin';

% RandomNoise
RandomNoise = logical(1);

% Avoid memory block
if (Paraview_output==1) & RandomNoise
    warning('Paraview output is not recommended for setups with random noise!')
    RandomNoise = logical(0);
end

% Output
Is64BIT     = logical(0); % if you are reading a 64 bit file (Juqueen)

% Initialize temperature
InitTemperature        = 0;

%==========================================================================
% DOMAIN PARAMETERS (DIMENSIONAL) [km, deg C]
%==========================================================================
W       =   6000;
L       =   10;
H       =   2048;

% Markers
nump_x  =   1024*3;  %for simulations
nump_y  =   2*3;
nump_z  =   256*3;

% No of particles in a grid cell
npart_x = 3;
npart_y = 3;
npart_z = 3;

% Model specific parameters
dx  =   W/(nump_x);
dy  =   L/(nump_y);
dz  =   H/(nump_z);
x_left  =   0;
y_front =   0;
z_bot   = -1988;

% Thicknesses
ThicknessAir        =   60; % air
ThicknessOL         =   80; % oceanic lith
ThicknessWC         =   15; % weak crust
ThicknessSC         =   20; % strong core

% Subduction locations
xs1 = 2000; % orig 2000
xs2 = 4000;

left_margin = 50; %unattached margin

% partial crust
partial_crust = 1;   % 0-no partial crust, 1-partial crust 2-no weak crust
crust_x       = 500; % 500 km no weak crust in upper plate

% Weak zone depth
weak_depth          =   170;
weak_angle          =   30;

% Angle of subduction
alpha       =  70; % 45,70

% Slab
slab_depth  = 300; % 300, 500

% Radius of curvature
radius      = 150; % whole radius 150, 250

% Marker positions
xmark1 =  250;
xmark2 = 2500;
xmark3 = 5500;

mark_thick = 80;

% Phase transitions
depth_lowermantle = 660; % depth to lower mantle (km)
depth_tranzitionz = 410; % depth to transition zone (km)

%==========================================================================
% MESH GRID
%==========================================================================

% Create new uniform grid
if LoadMesh == 0
    x = [x_left  + dx*0.5 : dx : x_left+W  - dx*0.5 ];
    y = [y_front + dy*0.5 : dy : y_front+L - dy*0.5 ];
    z = [z_bot   + dz*0.5 : dz : z_bot+H   - dz*0.5 ];
    [X,Y,Z] =   meshgrid(x,y,z);
    [Xq,Yq] =   meshgrid(x,y);
end

% Load grid from parallel partitioning file
if LoadMesh == 1
    [Xreg,Yreg,Zreg,x,y,z,X,Y,Z] = FDSTAGMeshGeneratorMatlab(npart_x,npart_y,npart_z,Parallel_partition, RandomNoise,Is64BIT );
    [Xq,Yq] = meshgrid(Xreg(1,:,1),Yreg(:,1,1));
    
    % Update other variables
    nump_x = size(Xreg,2);
    nump_y = size(Xreg,1);
    nump_z = size(Xreg,3);
end

%==========================================================================
% PHASES
%==========================================================================

%PHASES
mantle        = 0;
air           = 1;

% left subduction
slabLS        = 2;
weak_crustLS  = 3;
strong_coreLS = 4;

% right subduction
slabRS        = 5;
weak_crustRS  = 6;
strong_coreRS = 7;

% upper plate
slabUP        = 8;
strong_coreUP = 9;

% other
weak_zone     = 10;

% markers
mark1         = 11;
mark2         = 12;
mark3         = 13;

% phase transform weak
weak_left     = 14;
weak_right    = 15;

% mantle layers
lower_mantle  = 16; %lay2
lay3          = 17;
lay4          = 18;
lay5          = 19;
lay6          = 20;
lay7          = 21;
lay8          = 22;
lay9          = 23;
lay10         = 24;

Phase   =   zeros(size(X));     %   Contains phases

%==========================================================================
% SETUP GEOMETRY
%==========================================================================

% Plates
ind         =   find(Z>(H-ThicknessAir-ThicknessOL+z_bot));
Phase(ind)  =   slabUP;

DepthSC     =   ThicknessOL/2;
ind         =   find(Z>(H-ThicknessAir-(DepthSC+ThicknessSC/2)+z_bot) & Z<(H-ThicknessAir-(DepthSC-ThicknessSC/2)+z_bot));
Phase(ind)  =   strong_coreUP;

% 1. Left subduction
radiusS     =   radius-ThicknessOL;
radiusWC    =   radius-ThicknessWC;

xcenter     =   xs1;
zcenter     =   H-ThicknessAir-radius+z_bot;

%%% SLAB %%%
% points A,B of the hinge at alpha angle
xA = xcenter + cosd(90-alpha)*radiusS;
zA = zcenter + sind(90-alpha)*radiusS;

xB = xcenter + cosd(90-alpha)*radius;
zB = zcenter + sind(90-alpha)*radius;

m = (zA-zB)/(xA-xB);
b = zA - m*xA;

% hinge
ind         =   find( (X > xcenter) & (Z>m*X+b) & ((X-xcenter).^2+(Z-zcenter).^2<radius^2) & ((X-xcenter).^2+(Z-zcenter).^2>radiusS^2));
Phase(ind)  =   slabRS;

% downgoing part
ind         =   find( (X >= xA) & Z>=(H-ThicknessAir-slab_depth+z_bot) & (zA-Z)<=(X-xA)*tand(alpha) & Z<=zB & (zB-Z)>=(X-xB)*tand(alpha));
Phase(ind)  =   slabRS;

%%% WEAK CRUST %%%
% points A,B of the hinge at alpha angle
xA = xcenter + cosd(90-alpha)*radiusWC;
zA = zcenter + sind(90-alpha)*radiusWC;

xB = xcenter + cosd(90-alpha)*radius;
zB = zcenter + sind(90-alpha)*radius;

m = (zA-zB)/(xA-xB);
b = zA - m*xA;

% hinge
ind         =   find( (X > xcenter) & (Z>m*X+b) & ((X-xcenter).^2+(Z-zcenter).^2<radius^2) & ((X-xcenter).^2+(Z-zcenter).^2>radiusWC^2));
Phase(ind)  =   weak_crustRS;

% downgoing part
ind         =   find( (X >= xA) & Z>=(H-ThicknessAir-slab_depth+z_bot) & (zA-Z)<=(X-xA)*tand(alpha) & Z<=zB & (zB-Z)>=(X-xB)*tand(alpha));
Phase(ind)  =   weak_crustRS;

%%% STRONG CORE %%%
% points A,B of the hinge at alpha angle
xA = xcenter + cosd(90-alpha)*(radius-ThicknessOL/2-ThicknessSC/2);
zA = zcenter + sind(90-alpha)*(radius-ThicknessOL/2-ThicknessSC/2);

xB = xcenter + cosd(90-alpha)*(radius-ThicknessOL/2+ThicknessSC/2);
zB = zcenter + sind(90-alpha)*(radius-ThicknessOL/2+ThicknessSC/2);

m = (zA-zB)/(xA-xB);
b = zA - m*xA;

% hinge
radius1     =   radius-ThicknessOL/2-ThicknessSC/2;
radius2     =   radius-ThicknessOL/2+ThicknessSC/2;
ind         =   find( (X > xcenter) & (Z>m*X+b) & ((X-xcenter).^2+(Z-zcenter).^2<radius2^2) & ((X-xcenter).^2+(Z-zcenter).^2>radius1^2));
Phase(ind)  =   strong_coreRS;

% downgoing part
ind         =   find( (X >= xA) & Z>=(H-ThicknessAir-slab_depth+z_bot) & (zA-Z)<=(X-xA)*tand(alpha) & Z<=zB & (zB-Z)>=(X-xB)*tand(alpha));
Phase(ind)  =   strong_coreRS;

% % 2. Right subduction - copy and translate
ind         =   find( X>= xs1 & X < (xs2+xs1)/2);
ind2        =   find( X>= xs2 & X < xs2+(xs2-xs1)/2);
Phase(ind2) =   Phase(ind);

% Correct left subduction - erases all plates to the left
ind         =   find(X<xs2); 
Phase(ind)  =   mantle;

% Separate plates
ind         =   find(Z>(H-ThicknessAir-ThicknessOL+z_bot) & X<xs2);
Phase(ind)  =   slabRS;

ind         =   find(Z>(H-ThicknessAir-ThicknessWC+z_bot) & X<xs2 & X>=xs1+crust_x);
Phase(ind)  =   weak_crustRS;

DepthSC     =   ThicknessOL/2;
ind         =   find(Z>(H-ThicknessAir-(DepthSC+ThicknessSC/2)+z_bot) & Z<(H-ThicknessAir-(DepthSC-ThicknessSC/2)+z_bot) & X<xs2);
Phase(ind)  =   strong_coreRS;

% Add weak zones
xA          =   xs1+weak_depth*tand(weak_angle);
xB          =   xA-weak_depth*tand(weak_angle+5);

ind         =   find( (X >= xs1) & Z>=(H-ThicknessAir-weak_depth+z_bot) & (H-ThicknessAir-Z+z_bot)<=(xA-X)*tand(180-weak_angle)  & (H-ThicknessAir-Z+z_bot)>=-(xA-xB)/3+(xA-X)*tand(180-weak_angle-5) );
Phase(ind)  =   weak_zone;

ind         =   find(Z>(H-ThicknessAir-ThicknessOL+z_bot) & (H-ThicknessAir-Z+z_bot)>=(xA-X)*tand(180-weak_angle));
Phase(ind)  =   slabLS;

ind         =   find(Z>(H-ThicknessAir-ThicknessWC+z_bot) & (H-ThicknessAir-Z+z_bot)>=(xA-X)*tand(180-weak_angle));
Phase(ind)  =   weak_crustLS;

DepthSC     =   ThicknessOL/2;
ind         =   find(Z>(H-ThicknessAir-(DepthSC+ThicknessSC/2)+z_bot) & Z<(H-ThicknessAir-(DepthSC-ThicknessSC/2)+z_bot) & (H-ThicknessAir-Z+z_bot)>=(xA-X)*tand(180-weak_angle));
Phase(ind)  =   strong_coreLS;

% % Add transition zone
% ind         =   find( Z<(H-ThicknessAir-depth_tranzitionz+z_bot) );
% Phase(ind)  =   trans_zone;

% Add Lower mantle
ind         =   find( Z<(H-ThicknessAir-depth_lowermantle+z_bot) );
Phase(ind)  =   lower_mantle;

% Add AIR
ind         =   find( Z>(H-ThicknessAir+z_bot) );
Phase(ind)  =   air;

% Insert markers in the lower crust
ind         =   find(Phase==strong_coreLS & X>=xmark1 & X<xmark1+mark_thick);
Phase(ind)  =   mark1;

ind         =   find(Phase==strong_coreRS & X>=xmark2 & X<xmark2+mark_thick);
Phase(ind)  =   mark2;

ind         =   find(Phase==strong_coreUP & X>=xmark3 & X<xmark3+mark_thick);
Phase(ind)  =   mark3;

%==========================================================================
% TEMPERATURE - in Celcius
%==========================================================================
% Set initial temperature distribution (air) - in Celcius
Temp    =   zeros(size(X));

%==========================================================================
% PREPARE DATA FOR VISUALIZATION/OUTPUT
%==========================================================================

% Prepare data for visualization/output
A = struct('W',[],'L',[],'H',[],'nump_x',[],'nump_y',[],'nump_z',[],'Phase',[],'Temp',[],'x',[],'y',[],'z',[],'npart_x',[],'npart_y',[],'npart_z',[]);

Phase       = permute(Phase,[2 1 3]);
Temp        = permute(Temp, [2 1 3]);

% Linear vectors containing coords
x = X(1,:,1);
y = Y(:,1,1);
z = Z(1,1,:);

A.W      = W;
A.L      = L;
A.H      = H;
A.nump_x = nump_x;
A.nump_y = nump_y;
A.nump_z = nump_z;
A.Phase  = Phase;
A.Temp   = Temp;
A.x      = x(:);
A.y      = y(:);
A.z      = z(:);
A.npart_x= npart_x;
A.npart_y= npart_y;
A.npart_z= npart_z;

X        = permute(X,[2 1 3]);
Y        = permute(Y,[2 1 3]);
Z        = permute(Z,[2 1 3]);

A.Xpart  =  X;
A.Ypart  =  Y;
A.Zpart  =  Z;

% SAVE DATA IN 1 FILE (redundant)
if (LaMEM_Redundant_output == 1)
    PhaseVec(1) = nump_z;
    PhaseVec(2) = nump_y;
    PhaseVec(3) = nump_x;
    PhaseVec    = [PhaseVec(:); X(:); Y(:); Z(:); Phase(:); Temp(:)];
    
    % Save data to file
    ParticleOutput  =   'MarkersInput3D.dat';
    
    PetscBinaryWrite(ParticleOutput, PhaseVec);
    
end

% Clearing up some memory for parallel partitioning
clearvars -except A Paraview_output LaMEM_Parallel_output Parallel_partition Is64BIT RandomNoise

% PARAVIEW VISUALIZATION
if (Paraview_output == 1)
    if (RandomNoise)
        FDSTAGWriteMatlab2VTK(A,'VTU_BINARY'); % vtu binary for markers
    else
        FDSTAGWriteMatlab2VTK(A,'BINARY'); % default option - for regular mesh
    end
    %FDSTAGWriteMatlab2VTK(A,'ASCII'); % for debugging only (slow)
end

% SAVE PARALLEL DATA (parallel)
if (LaMEM_Parallel_output == 1)
    FDSTAGSaveMarkersParallelMatlab(A,Parallel_partition,Is64BIT);
end

%clear data
clear
